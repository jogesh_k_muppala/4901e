﻿
var mongoose = require('mongoose');
var config = require('./config');
var autoIncrement = require("mongoose-auto-increment");

var model = module.exports;
model.init = function (cb) {
    mongoose.connect(config.db);
    
    var db = mongoose.connection;
    db.on('error', function (err) {
        console.log('connection error', err);
    });
    db.once('open', function () {
        console.log('connected to db.');
        cb();
    });
    
    autoIncrement.initialize(db);
    
    model.db = db;
    
    model.users = require('./models/users.js')(db);
    model.events = require('./models/events.js')(db);
    model.eventRecords = require('./models/eventRecords.js')(db);
    model.eventSessions = require('./models/eventSessions.js')(db);
    model.ranks = require('./models/ranks.js')(db);
    model.eventRanks = require('./models/eventRanks.js')(db);
    model.staffs = require('./models/staffs.js')(db);
    model.eventRegs = require('./models/eventRegs.js')(db);

    /*var e = new model.events({ name_eng: "Test Event" });
    e.save(function(err, e) {
        var s = new model.eventSessions({ event: e._id, name_eng: "Test Session" });
        s.save(function(err, s) {
        });
    })*/
}


