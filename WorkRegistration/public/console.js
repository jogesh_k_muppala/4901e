var app = angular.module('homeScript', ["kendo.directives"]);

app.config(['$compileProvider',
    function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
    }]);

app.factory('global', ['$http', '$log', '$window', '$q', '$timeout',
    function ($http, $log, $window, $q, $timeout) {
        var o = {
            //Constant section
            timeout_val: parseInt("10000"), //10 seconds
            window_about_on: false,
            AssetGridExportFlag: false,
            import_fileUrl: "",
            request_timeout_obj: setTimeout(function () { $log.log("TIMEOUT") }, 1000 * 3600), //Timeout object (not $timeout)
            
            sidebar_treeData_SingleLayerItems: [
                "Event", "Volunteer", "Staff", 
                "Score Regulation", "Scheme Regulation",
                "Import", "Export", 
                "About", "Logout"
            ],
            
            sidebar_treeData_SingleLayerList: function (ListOfString) {
                var output = { data: [] };
                for (var i = 0; i < ListOfString.length; i++) {
                    output.data.push({
                        text: ListOfString[i]
                    });
                }
                return output;
            },
            
            gotoLink_sameWin: function (url) {
                //Redirect with AngularJS instead of HTML a herf
                $window.location.href = url;
            },
            
            gotoLink: function (url) {
                //Note: full url
                //var url = "http://" + $window.location.host + link;
                //$log.log(url);
                //$window.location.href = url;
                $window.open(url, '_blank');
            },            
            
            //Kendo error objects
            //From e.error() instead from server 
            xhr_response_obj: function (a, b, c) {
                return {
                    status: a,
                    statusText: b,
                    responseText: c
                };
            },
            
            request_fail_obj: function (kName, op) {
                return {
                    status: 607,
                    statusText: "Angular Mediator: Bad Request",
                    responseText: angular.toJson({ kName: kName, op: op })
                };
            },
            
            mediator_fail_obj: function (kName, op) {
                return {
                    status: 601,
                    statusText: "Angular Mediator: Fail Binding",
                    responseText: angular.toJson({ kName: kName, op: op })
                };
            },
            
            not_supported_obj: function (kName, op) {
                return {
                    status: 641,
                    statusText: "Angular Mediator: Binding success but operation is not supported",
                    responseText: angular.toJson({ kName: kName, op: op })
                };
            },
            
            not_json_obj: function (kName, op) {
                return {
                    status: 643,
                    statusText: "Angular Mediator: Server not returning JSON object",
                    responseText: angular.toJson({ kName: kName, op: op })
                };
            },
            
            //Kendo genereal event handlers
            customKendoDataSourceErrorHandler: function (e) {
                // e: Kendo event
                clearTimeout(o.request_timeout_obj);
                //if (angular.isDefined(o.request_timeout_obj.cancel)) { o.request_timeout_obj.cancel(); }
                //$log.log(e);
                if (!e.xhr) { e = { xhr: e }; }
                alert("Error code: " + e.xhr.status + " - " + e.xhr.statusText + "\n" +
                    "Error message: " + e.xhr.responseText);
                if (parseInt(e.xhr.status) == 401) { o.gotoLink("login"); }
            },
            
            //Remove vertical Scrollbar
            //http://docs.telerik.com/kendo-ui/web/grid/how-to/Layout/hide-scrollbar-when-not-needed
            kendoGridToggleScrollbar: function (e) {
                var gridWrapper = e.sender.wrapper;
                var gridDataTable = e.sender.table;
                var gridDataArea = gridDataTable.closest(".k-grid-content");
                
                gridWrapper.toggleClass("no-scrollbar", gridDataTable[0].offsetHeight < gridDataArea[0].offsetHeight);
            },
            
            //Dummy last item for copying convinence
            dummy: "dummy"
        };
        
        //More Kendo error objects
        o.xhr_timeout_obj = {
            status: 408,
            statusText: "Request Timeout",
            responseText: "Timeout amount(ms): " + o.timeout_val
        },
        
        //Side bar - sideTree
        
        //Kendo promise event with Angular callback functions and $http events
        //It acts like a $q but in Kendo style
        o.sidebar_treeData_offline = o.sidebar_treeData_SingleLayerList(o.sidebar_treeData_SingleLayerItems);
        
        o.sideTree_selectedText = "";
        o.sideTreeOptions = {
            //autoBind: false,
            //autoScroll: true,
            //dataSource: o.sidebar_treeData_online,
            dataSource: o.sidebar_treeData_offline,
            dataTextField: "text",
            //Instead of using MVVM method (Kendo render template, then bind angular event to HTML elemenets),
            //It could be simplified as "select" event.
            template: kendo.template($("#sideTreeTemplate").html()),
            select: function (e) {
                o.sideTree_selectedText = this.text(e.node);
                //Show selected item (node -> text)
                console.log("o.sideTreeOptions: Selecting ", o.sideTree_selectedText);
                //May place some logic to do show/ hide control
                //Since it may be not convinenent to place too many jQuery statements here,
                //The logic is placed in the $scope part (bottom of this code), with $scope.watch()
                if (o.sideTree_selectedText == "Logout") {
                    o.gotoLink_sameWin("logout");
                }
            },
            error: function (e) {
                o.customKendoDataSourceErrorHandler(e);
                //this.cancelChanges();
            }
        }
        
        //Kept for data tracing - will be excessive use of dataItem
        o.sidebar_tree_click = function (dataItem) {
            //$log.log("o.sidebar_tree_click: " + dataItem);
            return dataItem;
        }
        
        //Sidebar - About Window
        o.aboutWindowOptions = {
            //Content will be in HTML file
            title: "About",
            visible: false,
            width: 700, //minWidth: 240, maxHeight: 800,
            height: 350, //minHeight: 180, maxHeight: 600,
            actions: ["Close"], // ["Minimize", "Close"],
            autoFocus: true,
            pinned: true,
            //position: {top: "50%", left: "50%"},
            resizable: false
        }
        
        //Content - Grid
        
        //It acts like a $q but in Kendo style
        //Super mega angular wrapper for CRUD event!
        //kName: name of involved component in HTML (convension)
        //op: operation (c/r/u/d)
        //e: Kendo event object
        //dataItem: dataItem provided by Kendo
        o.server_access = function (kName, op, e, dI) {
            //return $q(function (resolve, reject) {
            
            //Make Timeout object if not exist; ignore if it's counting down
            //$log.log(o.request_timeout_obj);
            //if (!angular.isDefined(o.request_timeout_obj)) {
            clearTimeout(o.request_timeout_obj);
            $log.log("o.server_access(" + kName + "," + op + ")");
            o.request_timeout_obj = setTimeout(function () {
                //$log.log("Timeout");
                e.error(o.xhr_timeout_obj);
            }, o.timeout_val);
            //}
            
            //Some general local variable
            var http_link = ""; //o.node_server_addr;
            var post_obj = {};
            
            //Mega mediator
            //Call e.error(XHR response, status, errorthrown) for failure
            //Call e.success(data) for success
            if (kName == "eventGrid") {
                if (op == "c") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/addEvent";
                    
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "r") {
                    //$log.log(dI);
                    var http_link_read = http_link + "/api_darren/getEvent";
                    //var cateSub_list = [];
                    
                    $http.post(http_link_read, {}).success(function (data) {
                        var objs = [];
                        try { objs = angular.fromJson(data); } catch (err) { e.error(o.not_json_obj(kName, op), 0, ""); return; }
                        //$log.log(objs);
                        clearTimeout(o.request_timeout_obj);
                        e.success(objs);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "u") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/updateEvent";
                    //$log.log(post_obj);
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "d") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/deleteEvent";
                    //$log.log(post_obj);
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else if (kName == "checkRecordGrid") {
                if (op == "r") {
                    //Internal Check In/Out records
                    clearTimeout(o.request_timeout_obj);
                    e.success(dI.records);
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else if (kName == "eventSessionGrid") {
                if (op == "c") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/addEventSession";
                    e.data.event_id = dI._id;
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "r") {
                    //$log.log(dI);
                    var http_link_read = http_link + "/api_darren/getEventSession";
                    //var cateSub_list = [];
                    
                    $http.post(http_link_read, { event: dI._id }).success(function (data) {
                        var objs = [];
                        var objList = [];
                        try { objs = angular.fromJson(data); } catch (err) { e.error(o.not_json_obj(kName, op), 0, ""); return; }
                        for (var i = 0; i < objs.length; i++) {
                            var obj = {};
                            angular.copy(objs[i], obj);
                            
                            //event_name_eng: { from: 'event_name_eng', type: 'string', defaultValue: dataItem.name_eng, editable: false },
                            try {
                                obj.event_EventID = objs[i].event.EventID;
                            } catch (err) {
                                obj.event_EventID = "";
                            }
                            
                            objList.push(obj);
                        }
                        //$log.log(objs);
                        clearTimeout(o.request_timeout_obj);
                        e.success(objList);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "u") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/updateEventSession";
                    e.data.event_id = dI._id;
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "d") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/deleteEventSession";
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else if (kName == "userGrid") {
                if (op == "c") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/addUser";
                    
                    var post_obj = e.data;
                    post_obj.UST = {
                        stid: e.data.UST_stid,
                        itsc: e.data.UST_itsc,
                        school: e.data.UST_school,                     
                        yearOfStudy: e.data.UST_yearOfStudy,
                    };
                    post_obj.EmergencyContact = {
                        Name: e.data.EmergencyContact_Name,
                        Relationship: e.data.EmergencyContact_Relationship,
                        Tel: e.data.EmergencyContact_Tel,
                    };
                    //$log.log(post_obj);
                    $http.post(http_link_add, post_obj).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "r") {
                    //$log.log(dI);
                    var http_link_read = http_link + "/api_darren/getUser";
                    //var cateSub_list = [];
                    
                    $http.post(http_link_read, {}).success(function (data) {
                        var objs = [];
                        try { objs = angular.fromJson(data); } catch (err) { e.error(o.not_json_obj(kName, op), 0, ""); return; }
                        //$log.log(objs);
                        
                        var UserList = [];
                        for (var i0 = 0; i0 < objs.length; i0++) {
                            if (!objs[i0].UST) {
                                objs[i0].UST = {};
                            }
                            if (!objs[i0].EmergencyContact) {
                                objs[i0].EmergencyContact = {};
                            }
                            
                            var User = {};
                            angular.copy(objs[i0], User);
                            
                            User.UST_stid = objs[i0].UST.stid;
                            User.UST_itsc = objs[i0].UST.itsc;
                            User.UST_school = objs[i0].UST.school;
                            User.UST_yearOfStudy = objs[i0].UST.yearOfStudy;
                            User.EmergencyContact_Name = objs[i0].EmergencyContact.Name;
                            User.EmergencyContact_Relationship = objs[i0].EmergencyContact.Relationship;
                            User.EmergencyContact_Tel = objs[i0].EmergencyContact.Tel;
                            
                            try {
                                User.rank_name_eng = objs[i0].rank.name_eng;
                            } catch (err) {
                                User.rank_name_eng = "";
                            }
                            
                            UserList.push(User);
                        }
                        //$log.log(cateMain_list);
                        //$timeout.cancel(o.request_timeout_obj);
                        
                        clearTimeout(o.request_timeout_obj);
                        e.success(UserList);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "u") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/updateUser";
                    
                    var post_obj = e.data;
                    post_obj.UST = {
                        stid: e.data.UST_stid,
                        itsc: e.data.UST_itsc,
                        school: e.data.UST_school,                     
                        yearOfStudy: e.data.UST_yearOfStudy,
                    };
                    post_obj.EmergencyContact = {
                        Name: e.data.EmergencyContact_Name,
                        Relationship: e.data.EmergencyContact_Relationship,
                        Tel: e.data.EmergencyContact_Tel,
                    };
                    
                    //$log.log(post_obj);
                    $http.post(http_link_add, post_obj).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "d") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/deleteUser";
                    
                    //$log.log(post_obj);
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else if (kName == "rankGrid") {
                if (op == "c") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/addRank";
                    
                    var post_obj = e.data;
                    post_obj.points_subtotal = {
                        sub_total: e.data.points_subtotal_sub_total
                    };
                    //$log.log(post_obj);
                    $http.post(http_link_add, post_obj).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "r") {
                    //$log.log(dI);
                    var http_link_read = http_link + "/api_darren/getRank";
                    //var cateSub_list = [];
                    
                    $http.post(http_link_read, {}).success(function (data) {
                        var objs = [];
                        try { objs = angular.fromJson(data); } catch (err) { e.error(o.not_json_obj(kName, op), 0, ""); return; }
                        //$log.log(objs);
                        
                        var RankList = [];
                        for (var i0 = 0; i0 < objs.length; i0++) {
                            var Rank = {};
                            angular.copy(objs[i0], Rank);
                            if (objs[i0].points_subtotal.sub_level)
                                Rank.points_subtotal_sub_level_name = objs[i0].points_subtotal.sub_level.name_eng;
                            Rank.points_subtotal_sub_total = objs[i0].points_subtotal.sub_total;
                            
                            if (objs[i0].prereq) {
                                Rank.prereq_name = objs[i0].prereq.name_eng;
                            }
                            RankList.push(Rank);
                        }
                        //$log.log(RankList);
                        //$timeout.cancel(o.request_timeout_obj);
                        
                        clearTimeout(o.request_timeout_obj);
                        e.success(RankList);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "u") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/updateRank";
                    
                    var post_obj = e.data;
                    post_obj.points_subtotal = {
                        sub_total: e.data.points_subtotal_sub_total
                    };
                    //$log.log(post_obj);
                    $http.post(http_link_add, post_obj).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "d") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/deleteRank";
                    
                    //$log.log(post_obj);
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else if (kName == "eventRecordGrid") {
                if (op == "c") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/addEventRecord";
                    
                    //var post_obj = e.data;
                    //post_obj.event = dI;
                    //post_obj.user.name = e.data.userName;
                    var post_obj = e.data;
                    post_obj.session = dI;
                    /**
                    post_obj.user = {
                        name: e.data.user_name_eng
                    };
                    post_obj.eventRank = {
                        name_eng : e.data.eventRank_name_eng
                    };
                    **/
                    $log.log(post_obj);
                    $http.post(http_link_add, post_obj).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "r") {
                    //$log.log(dI);
                    var http_link_read = http_link + "/api_darren/getEventRecord";
                    //var cateSub_list = [];
                    
                    $http.post(http_link_read, { session: dI._id }).success(function (data) {
                        var objs = [];
                        try { objs = angular.fromJson(data); } catch (err) { e.error(o.not_json_obj(kName, op), 0, ""); return; }
                        $log.log(objs);
                        
                        var ERList = [];
                        for (var i0 = 0; i0 < objs.length; i0++) {
                            var ERecord = {};
                            angular.copy(objs[i0], ERecord);
                            
                            //session_name: { from: 'session_name', type: 'string', defaultValue: dataItem.name_eng, editable: false, validation: { required: true } },
                            if (objs[i0].session) {
                                ERecord.session_name = objs[i0].session.name_eng;
                            }
                            //user_name_eng: { from: 'user_name_eng', type: 'string', defaultValue: "", nullable: true },                          
                            if (objs[i0].user) {
                                ERecord.user_name_eng = objs[i0].user.name_eng;
                            }
                            //eventRank_name_eng: { from: 'eventRank_name_eng', type: 'string', defaultValue: "", nullable: true },
                            if (objs[i0].eventRank) {
                                ERecord.eventRank_name_eng = objs[i0].eventRank.name_eng;
                            }
                            if ((objs[i0].records) && (objs[i0].records.length > 0)) {
                                //signin_time: { from: 'signin_time', type: 'date', editable: false, nullable: true },
                                ERecord.signin_time = objs[i0].records[0].joinTime;
                                //signout_time: { from: 'sign_time', type: 'date', editable: false, nullable: true },
                                ERecord.signout_time = objs[i0].records[objs[i0].records.length - 1].leaveTime;
                            }
                            
                            ERList.push(ERecord);
                        }
                        $log.log(ERList);
                        //$timeout.cancel(o.request_timeout_obj);
                        
                        clearTimeout(o.request_timeout_obj);
                        e.success(ERList);
                    }).error(function (data, status, headers, config, statusText) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                    });
                } else if (op == "u") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/updateEventRecord";
                    
                    //var post_obj = e.data;
                    //post_obj.event = dI;
                    //post_obj.user.name = e.data.userName;
                    var post_obj = e.data;
                    post_obj.session = dI;
                    /**
                    post_obj.user = {
                        name: e.data.user_name_eng
                    };
                    post_obj.eventRank = {
                        name_eng : e.data.eventRank_name_eng
                    };
                    **/
                    //$log.log(post_obj);
                    $http.post(http_link_add, post_obj).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "d") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/deleteEventRecord";
                    
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else if (kName == "eventRankGrid") {
                if (op == "c") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/addEventRank";
                    
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "r") {
                    //$log.log(dI);
                    var http_link_read = http_link + "/api_darren/getEventRank";
                    //var cateSub_list = [];
                    
                    $http.post(http_link_read, {}).success(function (data) {
                        var objs = [];
                        try { objs = angular.fromJson(data); } catch (err) { e.error(o.not_json_obj(kName, op), 0, ""); return; }
                        //$log.log(objs);                      
                        
                        clearTimeout(o.request_timeout_obj);
                        e.success(objs);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "u") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/updateEventRank";
                    
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "d") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/deleteEventRank";
                    
                    //$log.log(post_obj);
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else if (kName == "staffGrid") {
                if (op == "c") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/addStaff";
                    
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "r") {
                    //$log.log(dI);
                    var http_link_read = http_link + "/api_darren/getStaff";
                    //var cateSub_list = [];
                    
                    $http.post(http_link_read, {}).success(function (data) {
                        var objs = [];
                        try { objs = angular.fromJson(data); } catch (err) { e.error(o.not_json_obj(kName, op), 0, ""); return; }
                        //$log.log(objs);                      
                        
                        clearTimeout(o.request_timeout_obj);
                        e.success(objs);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "u") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/updateStaff";
                    
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "d") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/deleteStaff";
                    
                    //$log.log(post_obj);
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else if (kName == "eventRegGrid") {
                if (op == "c") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/addEventReg";
                    
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "r") {
                    //$log.log(dI);
                    var http_link_read = http_link + "/api_darren/getEventReg";
                    //var cateSub_list = [];
                    
                    $http.post(http_link_read, { event: dI._id }).success(function (data) {
                        var objs = [];
                        var objList = [];
                        try { objs = angular.fromJson(data); } catch (err) { e.error(o.not_json_obj(kName, op), 0, ""); return; }
                        //$log.log(objs);                      
                        for (var i = 0; i < objs.length; i++) {
                            var obj = {};
                            angular.copy(objs[i], obj);
                            
                            //user_UST_stid: { from: 'user_UST_stid', type: 'string', defaultValue: "", validation: { required: true } },
                            try {
                                obj.user_UST_stid = objs[i].user.UST.stid;
                            } catch (err) {
                                obj.user_UST_stid = "";
                            }
                            
                            //event_EventID: { from: 'event_EventID', type: 'string', defaultValue: "", validation: { required: true } },
                            try {
                                obj.event_EventID = objs[i].event.EventID;
                            } catch (err) {
                                obj.event_EventID = "";
                            }
                            
                            objList.push(obj);
                        }
                        
                        clearTimeout(o.request_timeout_obj);
                        //e.success(objs);
                        e.success(objList);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "u") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/updateEventReg";
                    
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else if (op == "d") {
                    //Server have API already with validation of post body
                    //$log.log(e.data);
                    var http_link_add = http_link + "/api_darren/deleteEventReg";
                    
                    //$log.log(post_obj);
                    $http.post(http_link_add, e.data).success(function (data) {
                        //$timeout.cancel(o.request_timeout_obj);
                        clearTimeout(o.request_timeout_obj);
                        e.success(e.data);
                    }).error(function (data, status, headers, config) {
                        e.error(o.xhr_response_obj(status, kName + ", " + op, data), 0, "");
                        //e.error(o.request_fail_obj(kName, op), 0, "");
                    });
                } else {
                    e.error(o.mediator_fail_obj(kName, op), 0, "");
                }
            } else {
                e.error(o.mediator_fail_obj(kName, op), 0, "");
                return;
            }
            //});
        };
        
        o.eventGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("eventGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("eventGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("eventGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("eventGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                sort: { field: "date", dir: "lte" },
                pageSize: 20,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: "_id", type: 'string', editable: false, nullable: true },
                            EventID: { from: 'EventID', type: 'string', defaultValue: "", validation: { required: true } },
                            name_eng: { from: 'name_eng', type: 'string', defaultValue: "", validation: { required: true } },
                            name_chi: { from: 'name_chi', type: 'string', defaultValue: "", validation: { required: true } },
                            disabled: { from: 'disabled', type: 'boolean', defaultValue: false, nullable: true },
                        }
                    }
                }
            });
        };
        
        o.evnetGridOptions = function (dataItem) {
            return {
                dataSource: o.eventGridDataSource_online(dataItem),
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: "99%", //Better not dynamically - may oversize
                mobile: true,
                toolbar: ['create'],
                detailTemplate: kendo.template($("#eventGridDetail").html()),
                filterable: true,
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    { field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }               
                    //EventID: { from: 'EventID', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "EventID", title: "Event ID", width: "200px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //name_eng: { from: 'name_eng', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "name_eng", title: "Program Name (Eng)", width: "200px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //name_chi: { from: 'name_chi', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "name_chi", title: "Program Name (Chi)", width: "200px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //disabled: { from: 'disabled', type: 'boolean', defaultValue: false, nullable: true },
                    {
                        field: "disabled", title: "Disabled", width: "200px", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                ],
                
                //columnMenu: true,
                editable: {
                    //Be0tter define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    //template: kendo.template($("#assetGrid_editor").html()),
                    window: {
                        title: "Edit Event...",
                    }
                },               
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },              
            };
        };
        
        o.userGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("userGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("userGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("userGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("userGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                sort: { field: "name", dir: "asc" },
                pageSize: 20,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: "_id", type: 'string', editable: false, nullable: true },
                            name_eng: { from: "name_eng", type: 'string', defaultValue: "", validation: { required: true } },                           
                            name_chi: { from: "name_chi", type: 'string', defaultValue: "", nullable: true },
                            name_nick: { from: "name_nick", type: 'string', defaultValue: "", nullable: true },
                            gender: { from: "gender", type: 'string', defaultValue: "", nullable: true },
                            phoneNo: { from: 'phoneNo', type: 'number', defaultValue: 0, validation: { required: true } },
                            email: { from: 'email', type: 'string', defaultValue: "", validation: { required: true } },
                            UST_stid: { from: 'UST_stid', type: 'string', defaultValue: "", validation: { required: true } },
                            UST_itsc: { from: 'UST_itsc', type: 'string', defaultValue: "", validation: { required: true } },
                            UST_school: { from: 'UST_school', type: 'string', defaultValue: "", validation: { required: true } },
                            UST_yearOfStudy: { from: 'UST_yearOfStudy', type: 'string', defaultValue: "", validation: { required: true } },
                            Launguage: { from: 'Launguage', type: 'string', defaultValue: "", nullable: true },
                            EmergencyContact_Name: { from: 'EmergencyContact_Name', type: 'string', defaultValue: "", validation: { required: true } },
                            EmergencyContact_Relationship: { from: 'EmergencyContact_Relationship', type: 'string', defaultValue: "", validation: { required: true } },
                            EmergencyContact_Tel: { from: 'EmergencyContact_Tel', type: 'number', defaultValue: 0, validation: { required: true } },
                            UniYID: { from: 'UniYID', type: 'string', defaultValue: "", validation: { required: true } },
                            effectiveTo: { from: 'effectiveTo', type: "date", defaultValue: new Date() },
                            password: { from: 'password', type: 'string', defaultValue: "", validation: { required: true } },
                            token: { from: 'token', type: 'string', defaultValue: "", editable: false, nullable: true },
                            date: { from: 'date', type: "date", defaultValue: new Date(), nullable: true },
                            lastDate: { from: 'lastDate', type: "date", defaultValue: new Date(), nullable: true },
                            lastIP: { from: 'lastIP', type: 'string', defaultValue: "", nullable: true },                          
                            disabled: { from: 'disabled', type: 'boolean', defaultValue: false, nullable: true },
                            points: { from: 'points', type: 'number', defaultValue: 0, validation: { required: true } },
                            rank_name_eng: { from: 'rank_name_eng', type: 'string', defaultValue: "", nullable: true },
                            verified: { from: 'verified', type: 'boolean', defaultValue: false, nullable: true },
                        }
                    }
                }
            });
        };
        
        o.userGridOptions = function (dataItem) {
            return {
                dataSource: o.userGridDataSource_online(dataItem),
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: "99%", //Better not dynamically - may oversize
                mobile: true,
                toolbar: ['create'],
                filterable: true,
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    { field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }

                    //name_eng: { from: "name_eng", type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "name_eng", title: "English Name", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //name_chi: { from: "chi_name", type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "name_chi", title: "Chinese Name", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //name_nick: { from: "name_nick", type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "name_nick", title: "Nick Name", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //gender: { from: "gender", type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "gender", title: "Gender", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                   
                    //phoneNo: { from: 'phoneNo', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "phoneNo", title: "Phone Number", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //email: { from: 'email', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "email", title: "Email", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                  
                    //UST_stid: { from: 'UST_stid', type: 'string', defaultValue: 0, validation: { required: true } },
                    {
                        field: "UST_stid", title: "Student ID", width: "180px",
                        filterable: { cell: { operator: "contains" } }, 
                        attributes: { "class": "k-grid-td" }
                    },
                    //UST_itsc: { from: 'UST_itsc', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "UST_itsc", title: "ITSC", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //UST_school: { from: 'UST_school', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "UST_school", title: "School", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },             
                    //UST_yearOfStudy: { from: 'UST_yearOfStudy', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "UST_yearOfStudy", title: "Year Of Study", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                                      
                    //Launguage: { from: 'Launguage', type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "Launguage", title: "Launguage", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                   
                    //EmergencyContact_Name: { from: 'EmergencyContact_Name', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "EmergencyContact_Name", title: "Emergency Contact Person Name", width: "180px", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                           
                    //EmergencyContact_Relationship: { from: 'EmergencyContact_Relationship', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "EmergencyContact_Relationship", title: "Emergency Contact Person Relationship", width: "180px", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                            
                    //EmergencyContact_Tel: { from: 'EmergencyContact_Tel', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "EmergencyContact_Tel", title: "Emergency Contact Person Phone Number", width: "180px", hidden: true,
                        filterable: { cell: { operator: "contains" } }, format: "{0}",
                        attributes: { "class": "k-grid-td" }
                    },                                
                    //UniYID: { from: 'UniYID', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "UniYID", title: "UniY Membership No",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //effectiveTo: { from: 'effectiveTo', type: "date", defaultValue: new Date() },
                    {
                        field: "effectiveTo", title: "Effective Date To",
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd}",
                        attributes: { "class": "k-grid-td" }
                    },                   
                    //password: { from: 'password', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "password", title: "Password", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //date: { from: 'date', type: "date", defaultValue: new Date() },
                    {
                        field: "date", title: "Member since", hidden: true,
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //lastDate: { from: 'lastDate', type: "date", defaultValue: new Date() },
                    {
                        field: "lastDate", title: "Last GPS time", hidden: true,
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd HH:mm:ss}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //lastIP: { from: 'lastIP', type: 'string', defaultValue: "", nullable: true },        
                    {
                        field: "lastIP", title: "Last GPS IP", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //token: { type: String, index: { unique: true }, sparse: true },
                    {
                        field: "token", title: "Login Token", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //disabled: { from: 'disabled', type: 'boolean', defaultValue: false, validation: { required: true } },
                    {
                        field: "disabled", title: "Disabled", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //verified: { from: 'verified', type: 'boolean', defaultValue: false, nullable: true },
                    {
                        field: "verified", title: "Verified", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //points: { from: 'points', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "points", title: "Leftover Score", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //rank_name_eng: { from: 'rank_name_eng', type: 'string', defaultValue: "", nullable: true }
                    {
                        field: "rank_name_eng", title: "Award earned", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                ],
                
                //columnMenu: true,
                editable: {
                    //Better define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    template: kendo.template($("#userGridEditor").html()),
                    window: {
                        title: "Edit User...",
                    }
                },
                
                
                edit: function (e) {
                    e.container.find("input[name=password]").attr("type", "password");
                    
                    var window = e.container.data("kendoWindow");
                    //$log.log(e.container);
                    window.setOptions({ width: 840, height: 700 });
                    window.center();
                    //console.log($(".k-edit-form-container").wrapper).css({ width: 600 });
                    $(".k-edit-form-container").width("auto");
                    $(".k-edit-form-container").height("auto");
                    
                },
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },
                
            };
        };
        
        o.userGenderDataSource_offline = function (dataItem) {
            return [{ text: "Male", value: "Male" }, { text: "Female", value: "Female" }];
        };
        
        o.userGenderDropDownListOptions = function (dataItem) {
            return {
                //As stated in docuement, it can be shared with Kendo Grid's DataSource. Share it if there is common data.
                //But be careful for the arguments "dataItem" since the data flow may be different
                //dataSource: o.popup_category_dataSource,
                dataSource: o.userGenderDataSource_offline(dataItem),
                optionLabel: "--Select Gender--",
                dataTextField: "text",
                dataValueField: "value",
                select: function (e) {
                    var item = e.item;
                    var text = item.text();
                    // Use the selected item or its text
                    //$log.log(item);
                    //$log.log(text);
                }
            };
        };
        
        o.userLaunguageDataSource_offline = function (dataItem) {
            return [
                { text: "English", value: "English" }, 
                { text: "Cantonese", value: "Cantonese" }, 
                { text: "Mandarin", value: "Mandarin" }
            ];
        }
        
        o.userLaunguageDropDownListOptions = function (dataItem) {
            return {
                //As stated in docuement, it can be shared with Kendo Grid's DataSource. Share it if there is common data.
                //But be careful for the arguments "dataItem" since the data flow may be different
                //dataSource: o.popup_category_dataSource,
                dataSource: o.userLaunguageDataSource_offline(dataItem),
                optionLabel: "--Select Launguage--",
                dataTextField: "text",
                dataValueField: "value",
            };
        };
        
        o.userUSTSchoolDataSource_offline_full = function (dataItem) {
            return [
                { text: "School of Science", value: "School of Science" }, 
                { text: "School of Engineering", value: "School of Engineering" },
                { text: "School of Business and Management", value: "School of Business and Management" },
                { text: "School of Humanities and Social Science", value: "School of Humanities and Social Science" },
                { text: "Interdisciplinary Programs Office", value: "Interdisciplinary Programs Office" }
            ];
        };
        
        o.userUSTSchoolDataSource_offline = function (dataItem) {
            return [
                { text: "School of Science", value: "SSCI" }, 
                { text: "School of Engineering", value: "SENG" },
                { text: "School of Business and Management", value: "SBM" },
                { text: "School of Humanities and Social Science", value: "SOSC" },
                { text: "Interdisciplinary Programs Office", value: "IPO" }
            ];
        };
        
        o.userUSTSchoolDropDownListOptions = function (dataItem) {
            return {
                //As stated in docuement, it can be shared with Kendo Grid's DataSource. Share it if there is common data.
                //But be careful for the arguments "dataItem" since the data flow may be different
                //dataSource: o.popup_category_dataSource,
                dataSource: o.userUSTSchoolDataSource_offline(dataItem),
                optionLabel: "--Select School--",
                dataTextField: "text",
                dataValueField: "value",
            };
        };
        
        o.rankGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("rankGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("rankGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("rankGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("rankGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                //sort: { field: "date", dir: "lte" },
                pageSize: 20,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: "_id", type: 'string', editable: false, nullable: true },
                            name_eng: { from: 'name_eng', type: 'string', defaultValue: "", validation: { required: true } },
                            name_chi: { from: 'name_chi', type: 'string', defaultValue: "", validation: { required: true } },
                            token: { from: 'token', type: 'string', defaultValue: "", editable: false, nullable: true },
                            disabled: { from: 'disabled', type: 'boolean', defaultValue: false, nullable: true },
                            years: { from: 'years', type: 'number', defaultValue: 0, validation: { required: true } },
                            points_total: { from: 'points_total', type: 'number', defaultValue: 0, validation: { required: true } },
                            points_peryr: { from: 'points_peryr', type: 'number', defaultValue: 0, validation: { required: true } },
                            points_subtotal_sub_level_name: { from: 'points_subtotal_sub_level_name', type: 'string', defaultValue: "", nullable: true },
                            points_subtotal_sub_total: { from: 'points_subtotal_sub_total', type: 'number', defaultValue: 0, nullable: true },
                            prereq_name: { from: 'prereq_name', type: 'string', nullable: true }
                        }
                    }
                }
            });
        };
        
        o.rankGridOptions = function (dataItem) {
            return {
                dataSource: o.rankGridDataSource_online(dataItem),
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: "99%", //Better not dynamically - may oversize
                mobile: true,
                toolbar: ['create'],
                filterable: true,
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    { field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }
                
                    //_id: { from: "_id", type: 'string', editable: false, nullable: true },
                    {
                        field: "_id", title: "MongoID", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //name_eng: { from: 'name_eng', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "name_eng", title: "English Name", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //name_chi: { from: 'name_chi', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "name_chi", title: "Chinese Name (Optional)", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //token: { from: 'token', type: 'string', defaultValue: "", editable: false, nullable: true },
                    {
                        field: "token", title: "Token", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //disabled: { from: 'disabled', type: 'boolean', defaultValue: false, nullable: true },
                    {
                        field: "disabled", title: "Disabled", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //years: { from: 'years', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "years", title: "Years Required", hidden: true,
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //points_total: { from: 'points_total', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "points_total", title: "Points reqired (Total)", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },                   
                    //points_peryr: { from: 'points_peryr', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "points_peryr", title: "Points reqired (Per year)", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },   
                    //points_subtotal_sub_level_name: { from: 'points_subtotal_sub_level_name', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "points_subtotal_sub_level_name", title: "Event Rank Required", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //points_subtotal_sub_total: { from: 'points_subtotal_sub_total', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "points_subtotal_sub_total", title: "Points reqired (Event Rank)", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    }, 
                    //prereq_name: { from: 'prereq_name', type: 'string', nullable: true }
                    {
                        field: "prereq_name", title: "Rank Required", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                ],
                
                //columnMenu: true,
                editable: {
                    //Better define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    //template: kendo.template($("#assetGrid_editor").html()),
                    window: {
                        title: "Edit Rank...",
                    }
                },
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },
            };
        };
        
        o.eventSessionGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("eventSessionGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("eventSessionGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("eventSessionGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("eventSessionGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                //sort: { field: "date", dir: "lte" },
                pageSize: 20,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: "_id", type: 'string', editable: false, nullable: true },
                            SessionID: { from: "SessionID", type: 'string', defaultValue: "", nullable: true },
                            name_eng: { from: 'name_eng', type: 'string', defaultValue: "", validation: { required: true } },
                            name_chi: { from: 'name_chi', type: 'string', defaultValue: "", nullable: true },                     
                            startTime: { from: 'startTime', type: "date", defaultValue: new Date(), validation: { required: true } },
                            endTime: { from: 'endTime', type: "date", defaultValue: new Date(), validation: { required: true } },
                            maxHours: { from: 'maxHours', type: 'number', defaultValue: 0, nullable: true },
                            maxPoints: { from: 'maxPoints', type: 'number', defaultValue: 0, nullable: true },
                            location: { from: 'location', type: 'string', defaultValue: "", validation: { required: true } },
                            lat: { from: 'lat', type: 'number', defaultValue: 0, nullable: true },
                            lng: { from: 'lng', type: 'number', defaultValue: 0, nullable: true },
                            checkInToken: { from: 'checkInToken', type: 'string', defaultValue: "", editable: false, nullable: true },
                            checkOutToken: { from: 'checkOutToken', type: 'string', defaultValue: "", editable: false, nullable: true },
                            event_EventID: { from: 'event_EventID', type: 'string', defaultValue: dataItem.name_eng, editable: false }
                        }
                    }
                }
            });
        };
        
        o.eventSessionGridOptions = function (dataItem) {
            return {
                dataSource: o.eventSessionGridDataSource_online(dataItem),
                sortable: {
                    mode: "single", //"multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: 800, //Better not dynamically - may oversize
                mobile: true,
                detailTemplate: kendo.template($("#eventSessionGridDetail").html()),
                toolbar: ['create'],
                filterable: true,
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    { field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }
                    //event_EventID: { from: 'event_name_eng', type: 'string', defaultValue: "", validation: { required: true } }
                    {
                        field: "event_EventID", title: "Event ID", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },    
                    //SessionID: { from: "SessionID", type: 'string', editable: false, nullable: true },
                    {
                        field: "SessionID", title: "Session ID", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    }, 
                    //name_eng: { from: 'name_eng', type: 'string', defaultValue: "", validation: { required: true } },                            
                    {
                        field: "name_eng", title: "Session Name (English)", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //name_chi: { from: 'name_chi', type: 'string', defaultValue: "", validation: { required: true } },                            
                    {
                        field: "name_chi", title: "Session Name (Chinese)", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //startTime: { from: 'startTime', type: "date", defaultValue: new Date(), validation: { required: true } },
                    {
                        field: "startTime", title: "Start Time", width: "180px",
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd HH:mm:ss}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //endTime: { from: 'endTime', type: "date", defaultValue: new Date(), validation: { required: true } },
                    {
                        field: "endTime", title: "End Time", width: "180px",
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd HH:mm:ss}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //maxHours: { from: 'maxHours', type: 'number', defaultValue: 0, nullable: true },
                    {
                        field: "maxHours", title: "Service Hour Entitled", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },    
                    //maxPoints: { from: 'maxPoints', type: 'number', defaultValue: 0, nullable: true },
                    {
                        field: "maxPoints", title: "Score Entitled", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },    
                    //location: { from: 'location', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "location", title: "Location", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                    
                    //lat: { from: 'lat', type: 'number', defaultValue: 0, nullable: true },
                    {
                        field: "lat", title: "Latitude", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n6}",
                        attributes: { "class": "k-grid-td" }
                    },                    
                    //lng: { from: 'lng', type: 'number', defaultValue: 0, nullable: true },
                    {
                        field: "lng", title: "Longitude", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n6}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //checkInToken: { from: 'checkInToken', type: 'string', defaultValue: "", editable: false, nullable: true },
                    {
                        field: "checkInToken", title: "Check In Token", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },      
                    //checkOutToken: { from: 'checkOutToken', type: 'string', defaultValue: "", editable: false, nullable: true },
                    {
                        field: "checkOutToken", title: "Check Out Token", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                ],
                
                //columnMenu: true,
                editable: {
                    //Better define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    //template: kendo.template($("#eventRankGridEditor").html()),
                    window: {
                        title: "Edit Event Session...",
                    }
                },               
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },
            };
        };
        
        o.eventRecordGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("eventRecordGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("eventRecordGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("eventRecordGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("eventRecordGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                sort: { field: "joinTime", dir: "lte" },
                pageSize: 10,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: '_id', editable: false, nullable: true },
                            signin_time: { from: 'signin_time', type: 'date', editable: false, nullable: true },
                            signout_time: { from: 'sign_time', type: 'date', editable: false, nullable: true },
                            session_name: { from: 'session_name', type: 'string', defaultValue: dataItem.name_eng, editable: false, validation: { required: true } },
                            user_name_eng: { from: 'user_name_eng', type: 'string', defaultValue: "", nullable: true },
                            hours: { from: 'hours', type: 'number', defaultValue: 0, validation: { required: true } },
                            eventRank_name_eng: { from: 'eventRank_name_eng', type: 'string', defaultValue: "", nullable: true },
                            points: { from: 'points', type: 'number', defaultValue: 0, validation: { required: true } }
                        }
                    }
                }
            });
        };
        
        o.eventRecordGridOptions = function (dataItem) {
            return {
                dataSource: o.eventRecordGridDataSource_online(dataItem),
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: 800, //Better not dynamically - may oversize
                mobile: true,
                toolbar: ['create'],
                filterable: true,
                detailTemplate: kendo.template($("#eventRecordGridDetail").html()),
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    { field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }
                    //event_name_eng: { from: 'event_name_eng', type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "session_name", title: "Session Name", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //user_name_eng: { from: 'user_name_eng', type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "user_name_eng", title: "User Name (Eng)", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //signin_time: { from: 'signin_time', type: 'date', editable: false, nullable: true },
                    {
                        field: "signin_time", title: "First Check In Time", width: "180px",
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd HH:mm:ss}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //signout_time: { from: 'sign_time', type: 'date', editable: false, nullable: true },
                    {
                        field: "signout_time", title: "Last Check Out Time", width: "180px",
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd HH:mm:ss}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //hours: { from: 'hours', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "hours", title: "Hours count", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //eventRank_name_eng: { from: 'eventRank_name_eng', type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "eventRank_name_eng", title: "Event Rank Archieved (Eng)", width: "180px",
                        filterable: { cell: { operator: "contains" } }, 
                        attributes: { "class": "k-grid-td" }
                    },
                    //points: { from: 'points', type: 'number', defaultValue: 0, validation: { required: true } }
                    {
                        field: "points", title: "Points Earned", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },
                ],
                
                //columnMenu: true,
                editable: {
                    //Better define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    //template: kendo.template($("#assetGrid_editor").html()),
                    window: {
                        title: "Edit Event Record...",
                    }
                },
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },
            };
        };
        
        o.checkRecordGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("checkRecordGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("checkRecordGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("checkRecordGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("checkRecordGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                sort: { field: "joinTime", dir: "lte" },
                pageSize: 10,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: '_id', editable: false, nullable: true },
                            joinTime: { from: 'joinTime', type: "date", defaultValue: new Date() },
                            leaveTime: { from: 'leaveTime', type: "date", defaultValue: new Date() },
                            lat: { from: 'lat', type: 'number', defaultValue: 0, nullable: true },
                            lng: { from: 'lng', type: 'number', defaultValue: 0, nullable: true },
                        }
                    }
                }
            });
        };
        
        o.checkRecordGridOptions = function (dataItem) {
            return {
                dataSource: o.checkRecordGridDataSource_online(dataItem),
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: 800, //Better not dynamically - may oversize
                mobile: true,
                toolbar: ['create'],
                filterable: true,
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    //{ field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }
                    //joinTime: { from: 'joinTime', type: "date", defaultValue: new Date() },
                    {
                        field: "joinTime", title: "Check In Time", width: "180px",
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd HH:mm:ss}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //leaveTime: { from: 'leaveTime', type: "date", defaultValue: new Date() },
                    {
                        field: "leaveTime", title: "Check Out Time", width: "180px",
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd HH:mm:ss}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //lat: { from: 'lat', type: 'number', defaultValue: 0, nullable: true },
                    {
                        field: "lat", title: "Latitude", width: "180px", hidden: true,
                        filterable: { cell: { operator: "contains" } }, format: "{0:n6}",
                        attributes: { "class": "k-grid-td" }
                    },
                    //lng: { from: 'lng', type: 'number', defaultValue: 0, nullable: true },
                    {
                        field: "lng", title: "Longitude", width: "180px", hidden: true,
                        filterable: { cell: { operator: "contains" } }, format: "{0:n6}",
                        attributes: { "class": "k-grid-td" }
                    },
                ],
                
                //columnMenu: true,
                editable: {
                    //Better define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    //template: kendo.template($("#assetGrid_editor").html()),
                    window: {
                        title: "Edit Event Record...",
                    }
                },
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },
            };
        };
        
        o.eventRankGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("eventRankGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("eventRankGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("eventRankGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("eventRankGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                //sort: { field: "date", dir: "lte" },
                pageSize: 20,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: "_id", type: 'string', editable: false, nullable: true },
                            name_eng: { from: 'name_eng', type: 'string', defaultValue: "", validation: { required: true } },
                            name_chi: { from: 'name_chi', type: 'string', defaultValue: "", nullable: true },                    
                            disabled: { from: 'disabled', type: 'boolean', defaultValue: false, nullable: true },
                            points_perhr: { from: 'points_perhr', type: 'number', defaultValue: 0, validation: { required: true } },
                        }
                    }
                }
            });
        };
        
        o.eventRankGridOptions = function (dataItem) {
            return {
                dataSource: o.eventRankGridDataSource_online(dataItem),
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: "99%", //Better not dynamically - may oversize
                mobile: true,
                toolbar: ['create'],
                filterable: true,
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    { field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }
                    //name_eng: { from: 'name_eng', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "name_eng", title: "English Name", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //name_chi: { from: 'name_chi', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "name_chi", title: "Chinese Name (Optional)", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //disabled: { from: 'disabled', type: 'boolean', defaultValue: false, nullable: true },
                    {
                        field: "disabled", title: "Disabled", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                    //points_perhr: { from: 'points_perhr', type: 'number', defaultValue: 0, validation: { required: true } },
                    {
                        field: "points_perhr", title: "Points per hour", width: "180px",
                        filterable: { cell: { operator: "contains" } }, format: "{0:n1}",
                        attributes: { "class": "k-grid-td" }
                    },
                ],
                
                //columnMenu: true,
                editable: {
                    //Better define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    //template: kendo.template($("#eventRankGridEditor").html()),
                    window: {
                        title: "Edit Event Rank...",
                    }
                },
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },
            };
        };
        
        o.staffGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("staffGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("staffGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("staffGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("staffGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                //sort: { field: "StaffID", dir: "asc" },
                pageSize: 20,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: "_id", type: 'string', editable: false, nullable: true },
                            StaffID: { from: 'StaffID', type: 'string', defaultValue: "", nullable: true },
                            StaffName: { from: 'StaffName', type: 'string', defaultValue: "", nullable: true },
                            username: { from: 'username', type: 'string', defaultValue: "", validation: { required: true } },
                            salt: { from: 'salt', type: 'string', editable: false, nullable: true },
                            hash: { from: 'hash', type: 'string', defaultValue: "", validation: { required: true } }
                        }
                    }
                }
            });
        };
        
        o.staffGridOptions = function (dataItem) {
            return {
                dataSource: o.staffGridDataSource_online(dataItem),
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: "99%", //Better not dynamically - may oversize
                mobile: true,
                toolbar: ['create'],
                detailTemplate: kendo.template($("#eventGridDetail").html()),
                filterable: true,
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    { field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }
                    //_id: { from: "_id", type: 'string', editable: false, nullable: true },
                    {
                        field: "_id", title: "MongoID", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                      
                    //StaffID: { from: 'StaffID', type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "StaffID", title: "Staff ID", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },     
                    //StaffName: { from: 'StaffName', type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "StaffName", title: "Staff Name", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },     
                    //username: { from: 'username', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "username", title: "Login User Name", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },    
                    //salt: { from: 'salt', type: 'string', editable: false, nullable: true },
                    {
                        field: "salt", title: "Salt", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },   
                    //hash: { from: 'hash', type: 'string', defaultValue: "", validation: { required: true } }
                    {
                        field: "hash", title: "Encrypted Password", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    }
                ],
                
                //columnMenu: true,
                editable: {
                    //Be0tter define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    //template: kendo.template($("#assetGrid_editor").html()),
                    window: {
                        title: "Edit Staff...",
                    }
                },
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },
            };
        };
        
        o.eventRegGridDataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.server_access("eventRegGrid", "c", e, dataItem); },
                    read: function (e) { o.server_access("eventRegGrid", "r", e, dataItem); },
                    update: function (e) { o.server_access("eventRegGrid", "u", e, dataItem); },
                    destroy: function (e) { o.server_access("eventRegGrid", "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        //$log.log(options);
                        if (operation !== "read") {
                            //$log.log("PM");
                            return angular.toJson(options);
                        }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                //sort: { field: "StaffID", dir: "asc" },
                pageSize: 20,
                schema: {
                    //data: "items", //See server API
                    //total: "itemCount", //See server API
                    model: {
                        id: "_id",
                        fields: {
                            _id: { from: "_id", type: 'string', editable: false, nullable: true },
                            user_UST_stid: { from: 'user_UST_stid', type: 'string', defaultValue: "", validation: { required: true } },
                            event_EventID: { from: 'event_EventID', type: 'string', defaultValue: "", validation: { required: true } },
                            no: { from: 'no', type: 'string', defaultValue: "", nullable: true },
                            amount: { from: 'amount', type: 'number', defaultValue: 0, nullable: true },
                            date: { from: 'date', type: 'date', defaultValue: new Date(), nullable: true },
                            tee_size: { from: 'tee_size', type: 'string', defaultValue: "", nullable: true },
                        }
                    }
                }
            });
        };
        
        o.eventRegGridOptions = function (dataItem) {
            return {
                dataSource: o.eventRegGridDataSource_online(dataItem),
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: 800, //Better not dynamically - may oversize
                mobile: true,
                toolbar: ['create'],
                filterable: true,
                columns: [
                    //Comment out when don't need
                    //Command bar for admin
                    { field: "admin_actions", command: ["edit", "destroy"], title: "Admin actions", width: "120px" },
                    //Command bar for user
                    //{ field: "user_actions", command: [{ name: "edit", text: { edit: "View", cancel: "Close" } }], title: "User actions", width: "120px" }
                    //_id: { from: "_id", type: 'string', editable: false, nullable: true },
                    {
                        field: "_id", title: "MongoID", hidden: true,
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                      
                    //user_UST_stid: { from: 'user_UST_stid', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "user_UST_stid", title: "Volunteer's STID", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },   
                    //event_EventID: { from: 'event_EventID', type: 'string', defaultValue: "", validation: { required: true } },
                    {
                        field: "event_EventID", title: "EventID", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                     
                    //no: { from: 'no', type: 'string', defaultValue: "", nullable: true },
                    {
                        field: "no", title: "Receipt No.", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                       
                    //amount: { from: 'amount', type: 'number', defaultValue: 0, nullable: true },
                    {
                        field: "amount", title: "Receipt Amount", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },                                    
                    //date: { from: 'date', type: 'date', defaultValue: new Date(), nullable: true },
                    {
                        field: "date", title: "Receipt Date", width: "180px",
                        filterable: { cell: { operator: "lte" } }, format: "{0: yyyy-MM-dd}",
                        attributes: { "class": "k-grid-td" }
                    },                         
                    //tee_size: { from: 'tee_size', type: 'number', defaultValue: 0, nullable: true },
                    {
                        field: "tee_size", title: "Tee-shirt Size", width: "180px",
                        filterable: { cell: { operator: "contains" } },
                        attributes: { "class": "k-grid-td" }
                    },
                ],
                
                //columnMenu: true,
                editable: {
                    //Be0tter define them well in schema instad of hardcore pop-up template
                    mode: "popup",
                    confirmation: true,
                    //template: kendo.template($("#assetGrid_editor").html()),
                    window: {
                        title: "Edit Event Registration...",
                    }
                },               
                
                cancel: function (e) {
                    //alert("cancel");
                    //Fired when you click "Cancel" in edit window
                    //Override to emliate bug "row disappear when cancel"
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                    //$log.log("o.assetGridOptions: On cancel");
                },
            };
        };
        
        o._base64ToArrayBuffer = function (base64) {
            var binary_string = window.atob(base64);
            var len = binary_string.length;
            var bytes = new Uint8Array(len);
            for (var i = 0; i < len; i++) {
                bytes[i] = binary_string.charCodeAt(i);
            }
            return bytes.buffer;
        }
        
        //e: kendo event
        //op: operation code (see HTML)
        o.assetUploadModifyURL = function (e, dataItem, op) {
            var param;
            $log.log(dataItem);
            switch (op) {
                case 1: param = '?siteId=' + o.siteId 
                    + '&category_name=' + dataItem.category_name 
                    + "&subcategory_name=" + dataItem.subcategory_name; break;
                case 2: param = '?siteId=' + o.siteId 
                    + '&category_name=' + dataItem.category_name; break;
                case 3: param = '?siteId=' + o.siteId 
                    + '&tag_id=' + dataItem.tag_id; break;
                default:
                    alert("AssetUploadDocs: Operation code undefined");
                    $log.log("AssetUploadDocs: Operation code undefined");
            }
            e.sender.options.async.saveUrl = 'saveDoc' + param;
            e.sender.options.async.removeUrl = 'removeDoc' + param;
        };
        
        o.ImportUploadDocsOptions = function (dataItem, op) {
            return {
                //This async link cannot be declared as a function - if the parameter goes dynamic, 
                //Modify
                async: {
                    saveUrl: '/api_darren/importExcel?a=b',
                    removeUrl: 'undoimportExcel',
                    autoUpload: false
                },
                select: function (e) {
                    //This is triggered when user selected the files to upload
                    //Useful for limiting files number/ extension and cancel changes
                    //$log.log(dataItem);
                    //$log.log(e.files);
                    if (e.files.length > 1) {
                        alert("Please select 1 file only.");
                        e.preventDefault();
                    }
                    if (e.files[0].extension.indexOf("xls") < 0) {
                        alert("Only *.xls or *.xlsx is supported.");
                        e.preventDefault();
                    }
                },
                
                upload: function (e) {
                    //This is triggered before calling the add url and the success event
                    //$log.log("o.AssetUploadDocsOptions: onUpload()");
                    //$log.log(dataItem);
                    //Since dataItem.folder_name is set after initialization, urls are needed to be changed
                    //o.assetUploadModifyURL(e, dataItem, op);                  
                    var action = {
                        import_log: $("#import_log").prop("checked"),
                        import_excel: $("#import_excel").prop("checked"),
                        import_db: $("#import_db").prop("checked"),
                    }
                    if (!confirm("Are you going to proceed? \n " + op + angular.toJson(action))) {
                        e.preventDefault();
                    }
                    var code = prompt("Please type 'Proceed' to continue.");
                    if (code != "Proceed") {
                        e.preventDefault();
                    }
                    e.sender.options.async.saveUrl = '/api_darren/importExcel?';
                    if (action.import_log) {
                        e.sender.options.async.saveUrl += "import_log=true&"
                    }
                    if (action.import_excel) {
                        e.sender.options.async.saveUrl += "import_excel=true&"
                    }
                    if (action.import_db) {
                        e.sender.options.async.saveUrl += "import_db=true&"
                    }
                },
                
                error: function (e) {
                    //$log.log("o.AssetUploadDocsOptions: onError()");
                    alert(e.XMLHttpRequest.responseText);
                    //$log.log(e);
                },
                /**
                remove: function (e) {
                    //This is trigerred when user press the x button to cancel adding uploaded files.
                    //Note that this is called before calling the remove url and the success event.
                    //$log.log("o.AssetUploadDocsOptions: onRemove()");
                    o.assetUploadModifyURL(e, dataItem, op);
                },
                **/
                success: function (e) {
                    //$log.log("upload success");
                    //$log.log(e.sender.$angular_scope.dataItem);
                    //e.sender.$angular_scope.dataItem.photo_url == e.files[0].name;
                    o.post_upload("importExcel", e, dataItem, this, op);
                }
            }
        };
        
        //post-upload process such as auto-complete
        //kName: caller of this function (who call this?)
        //e: kendo event from the edit form. Uploaded files are usually in e.files 
        //dI: dataItem from the edit form.
        //inst: instance of the edit form. For potential use
        //op: operation or function name
        o.post_upload = function (kName, e, dI, inst, op) {
            //$log.log(inst);
            //Only visual stuffs (with data binding) is effective
            //Need to override Grid.save() event to make sure it calls $http
            
            //For visual elements, if http elements (e.g. input) is changed manually,
            //Grid.save() should be overrided to manually add the server access.
            //For data flow, change dI to make changes effective. dI is usually binded well in Kendo Grid.
            //Also, changing dI will not reflect changes visually. It could be tackled by Kendo's MVVM,
            //But unfourtunately, it's too difficult. 
            if (kName == "photo") {
                var http_link = o.node_server_addr 
                    + "/resources/" 
                    + o.siteId + "/" 
                    + dI.tag_id + "/" 
                    + e.files[0].name;
                //dI is shared by Angular scope and then bind to Kendo dataSource
                dI.photo_url = e.files[0].name;
                //But it's not binded into Kendo/HTML elements, need to do this manually
                $("#field_photo_url")[0].value = e.files[0].name;
                $("#field_photo_preview")[0].src = http_link;
                //Remember to override the save() event of the parent grid to call API function!
            } else if (kName == "doc") {
                var http_link = o.node_server_addr 
                    + "/resources/" 
                    + o.siteId + "/" 
                    + dI.tag_id + "/" 
                    + e.files[0].name;
                dI.link = http_link;
                $("#field_doc_link")[0].value = e.files[0].name;
            } else if (kName == "docs") {
                //Luckily (after "doc" and "photo"), this case ("docs") doesn't need custom visual update
                //Instead of updating the whole Asset, APIs will be called to modify the file list in the server.
                //Changing dI (UI side) and calling APIs (server side) 
                //Will make the changes be saved in the middle of the Asset editing progress.
                //Therefore no server access when in window close (That update the whole Asset instad of partially)
                
                //Disable the pull down list (lock the folder selection) to prevent potential risk
                //$("#UploadDropDownList").data("kendoDropDownList").enable(false);
                //dI already includes all the data of the current Asset/ Subcategory
                if (!dI.folder_name) { $log.log("o.post_upload: folder_name is missing"); return; }
                var doc_link = //o.node_server_addr
                    //+ "/resources/"
                    //+ o.siteId + "/"
                    //+ dI.tag_id + "/"
                    //+ e.files[0].name;
 e.files[0].name;
                
                //Since all cases involve server access, add the timer
                clearTimeout(o.request_timeout_obj);
                o.request_timeout_obj = setTimeout(function () {
                    o.customKendoDataSourceErrorHandler({ xhr: o.oXhr_timeout_obj });
                }, o.timeout_val);
                
                if (e.operation == "upload") {
                    //This ensure the Asset Grid (UI) is in sync with this distinct window.
                    //dI[dI.folder_name].push({ name: e.files[0].name, link: doc_link });
                    //This ensure the Server will in sync with the new file list
                    //var http_link_add = "/addDocToAsset";
                    var http_link_add = "addDocToCategory";
                    var post_obj_add = {
                        siteId: o.siteId,
                        tag_id: dI.tag_id,
                        //field_name: dI.folder_name,
                        category_name: dI.category_name,
                        subcategory_name: dI.subcategory_name,
                        doc_name: dI.folder_name, //e.files[0].name,
                        doc_link: doc_link
                    }
                    $http.post(http_link_add, post_obj_add).success(function (data) {
                        clearTimeout(o.request_timeout_obj);
                        o.refresh_assetFile(op);
                        //$log.log("o.post_upload: add file success");
                    }).error(function (data, status, headers, config) {
                        o.customKendoDataSourceErrorHandler(
                            { xhr: o.request_fail_obj("kendoupload", "c") }
                        );
                    });
                } else if (e.operation == "remove") {
                    var file_found = false;
                    //This ensure the Asset Grid (UI) is in sync with this distinct window.                
                    //for (var i0 = 0; i0 < dI[dI.folder_name].length; i0++) {
                    //    if (dI[dI.folder_name][i0].name == e.files[0].name) {
                    //        file_found = true;
                    //        dI[dI.folder_name].splice(i0, 1);
                    //    }
                    //}
                    //This ensure the Server will in sync with the new file list
                    var http_link_remove = "removeDocFromCategory";
                    var post_obj_remove = {
                        siteId: o.siteId,
                        tag_id: dI.tag_id,
                        //field_name: dI.folder_name,
                        category_name: dI.category_name,
                        subcategory_name: dI.subcategory_name,
                        doc_name: e.files[0].name,
                        doc_link: doc_link
                    };
                    $http.post(http_link_remove, post_obj_remove).success(function (data) {
                        clearTimeout(o.request_timeout_obj);
                        o.refresh_assetFile(op);
                        //$log.log("o.post_upload: remove file success");
                    }).error(function (data, status, headers, config) {
                        o.customKendoDataSourceErrorHandler(
                            { xhr: o.request_fail_obj("kendoupload", "d") }
                        );
                    });
                    //Error case: file not found in dI - send console message (or alert?)
                    if (!file_found) { $log.log("o.post_upload: remove target not found.") }
                } else {
                    $log.log("o.post_upload: uncaught situation")
                    $log.log(e);
                }
            } else if (kName == "docs") {
                alert("Upload complete.");
            } else if (kName == "importExcel") {
                if (e.response.ErrorFromDB) {
                    alert(e.response.ErrorFromDB);
                }
                $("textarea#import_output").val("Success: " + e.response.Success + "\n" 
                    + "File Size: " + e.response.File.length + "\n" 
                    + "Message: \n" + angular.toJson(e.response.Log, 2));
                if (e.response.File) {
                    var fBlob = new Blob([o._base64ToArrayBuffer(e.response.File)], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                    //var file = new File([fBlob], "name");
                    o.import_fileUrl = (window.URL || window.webkitURL).createObjectURL(fBlob);
                    //$log.log(o.import_fileUrl);
                    angular.element("#import_file").scope().$apply(function (scope) {
                        scope.import_fileUrl = o.import_fileUrl;
                    });
                }
                //alert("Upload complete. Now the Database is updated. See the grids to see effect.");
            } else {
                return; //Do nothing if not supported
            }
        }
        
        //Return service object
        return o;
    }]);

app.controller('Asset1Ctrl', [
    '$scope',
    '$http',
    '$interval',
    '$log',
    'global',
    function ($scope, $http, $log, $interval, global) {
        //Check login for every 60 seconds
        //clearInterval($scope.login_interval);
        //$scope.login_interval = setInterval(function () { global.bIsLoggedIn(); }, 1000 * 60);
        //Check user type
        //global.sUserType();
        
        //To make log in service/ factory visiable
        //In controller, just type console.log is OK
        $scope.$log = global.$log;
        
        //To pass the dataItem through Angular's binding
        $scope.dataItem = {};
        
        //Dummy DataSource
        $scope.nothing = [];
        
        //Kendo Window
        $scope.aboutWindowOptions = global.aboutWindowOptions;
        //Kendo TreeView
        $scope.sideTreeOptions = global.sideTreeOptions;
        $scope.sidebar_tree_click = global.sidebar_tree_click;
        
        //Kendo DataGrid      
        $scope.evnetGridOptions = global.evnetGridOptions;
        $scope.userGridOptions = global.userGridOptions;
        $scope.rankGridOptions = global.rankGridOptions;
        $scope.eventRankGridOptions = global.eventRankGridOptions;
        $scope.eventRecordGridOptions = global.eventRecordGridOptions;
        $scope.ImportUploadDocsOptions = global.ImportUploadDocsOptions;
        $scope.userGenderDropDownListOptions = global.userGenderDropDownListOptions;
        $scope.userLaunguageDropDownListOptions = global.userLaunguageDropDownListOptions;
        $scope.userUSTSchoolDropDownListOptions = global.userUSTSchoolDropDownListOptions;
        $scope.eventSessionGridOptions = global.eventSessionGridOptions;
        $scope.checkRecordGridOptions = global.checkRecordGridOptions;
        $scope.staffGridOptions = global.staffGridOptions;
        $scope.eventRegGridOptions = global.eventRegGridOptions;
        
        //Instead of $apply, since the value is changed by Kendo, it's not in Angular loop.
        //Therefore, if you want to watch it, use $scope.$watch(fReturn($scope),fChange(new))
        $scope.$watch(
            function () { return (global.sideTree_selectedText != null) ? (global.sideTree_selectedText) : (null); },
      function (newValue) {
                $scope.show_eventGrid = false;
                $scope.show_userGrid = false;
                $scope.show_rankGrid = false;
                $scope.show_staffGrid = false;
                $scope.show_eventRankGrid = false;
                $scope.show_importTab = false;
                $scope.show_exportTab = false;
                if (newValue == "About") { $scope.aboutWindow.open().center(); }
                else if (newValue == "Event") {
                    $scope.show_eventGrid = true;
                    $scope.eventGrid.setDataSource(global.eventGridDataSource_online({}));
                    $scope.eventGrid.refresh();
                }
                else if (newValue == "Volunteer") {
                    $scope.show_userGrid = true;
                    $scope.userGrid.setDataSource(global.userGridDataSource_online({}));
                    $scope.userGrid.refresh();
                }
                else if (newValue == "Staff") {
                    $scope.show_staffGrid = true;
                    $scope.staffGrid.setDataSource(global.staffGridDataSource_online({}));
                    $scope.staffGrid.refresh();
                }
                else if (newValue == "Scheme Regulation") {
                    $scope.show_rankGrid = true;
                    $scope.rankGrid.setDataSource(global.rankGridDataSource_online({}));
                    $scope.rankGrid.refresh();
                }
                else if (newValue == "Score Regulation") {
                    $scope.show_eventRankGrid = true;
                    $scope.eventRankGrid.setDataSource(global.eventRankGridDataSource_online({}));
                    $scope.eventRankGrid.refresh();
                }
                else if (newValue == "Import") {
                    $scope.show_importTab = true;
                }
                else if (newValue == "Export") {
                    $scope.show_exportTab = true;
                }
                else if (newValue != null) {
                    console.log("Uncaught command: " + newValue);
                }
            }
        );
        
        //Scope-Service function binding
        $scope.gotoLink = global.gotoLink;
        $scope.getFilename = global.getFilename;
        $scope.remove_doc_link = global.remove_doc_link;
        $scope.remove_photo = global.remove_photo;
        $scope.showLinkEx = global.showLinkEx;
        $scope.gotoLinkEx = global.gotoLinkEx;
        $scope.gen_cert_all = function () { alert("Coming soon"); }
        $scope.qr = function (token) { return "qr/" + token; }
        
        $scope.OpenAssetDocsUploadWindow = global.OpenAssetDocsUploadWindow;
        
        //Import and Export Stuffs
        $scope.export_VL_school = "";
        $scope.export_VL_gender = "";
        $scope.export_VL_nameEng = "";
        $scope.export_VL_Export = function () {
            var param = {
                School: $scope.export_VL_school,
                Gender: $scope.export_VL_gender,
                English_Name: $scope.export_VL_nameEng
            };
            global.gotoLink("/exports/users/list/" + param.School);
        };
        
        $scope.export_SL_Code = "";
        $scope.export_SL_Export = function () {
            var param = {
                Code: $scope.export_SL_Code,
            };
            global.gotoLink("/exports/events/list/" + param.Code);
        };
        
        $scope.export_VA_From = new Date();
        $scope.export_VA_To = new Date();
        $scope.export_VA_STID = "";
        $scope.export_VA_Export = function () {
            var param = {
                From: $scope.export_VA_From,
                To: $scope.export_VA_To,
                STID: $scope.export_VA_STID
            };
            global.gotoLink("/exports/eventRecords/list/" + param.From + "/" + param.To + "/" + param.STID);
        };
        
        $scope.export_VC_Code = "";
        $scope.export_VC_Export = function () {
            var param = {
                Code: $scope.export_SL_Code,
            };
            global.gotoLink("/exports/eventRegs/list/" + param.Code);
        };
        
        $scope.import_log = false;
        $scope.import_excel = false;
        $scope.import_db = false;
        $scope.import_output = "";
        
        $scope.import_fileUrl = "";
        $scope.$watch(
            function () { return global.import_fileUrl; },
            function (newValue) {
                console.log("BLOB");
                $scope.import_fileUrl = newValue;
            }
        );
    }
]);

app.controller('LoginCtrl', [
    '$scope',
    '$window',
    function ($scope, $window) {
        $scope.username = "";
        $scope.password = "";
        $scope.message = "";
        $scope.login = function (username, password) {
        //var url = "http://" + $window.location.host + "/web_console/asset4_asdDB.html";
        //console.log(angular.toJson([username, password]));
        //if (username == "admin") { $scope.gotoConsole(url); }
        }
        $scope.gotoConsole = function (url) {
        //$log.log(url);
        //$window.location.href = url;
        }
    }]);
