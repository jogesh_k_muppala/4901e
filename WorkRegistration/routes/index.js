﻿var express = require('express');
var router = express.Router();
var models = require('../models.js');
var qr = require('qr-image');
var config = require('../config');


// User authentication (web side only)
var passport = require('passport');
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);

/* GET home page. */
/**
router.get('/', function (req, res) {
    models.events.find({}, function (err, events) {
        if (err) next(err);
        else {
            models.users.find({}, function (err, users) {
                if (err) next(err);
                else {
                    res.render('index', { title: 'Express', events: events, users: users });
                }
            });
        }
    });
});
**/
router.get('/', function (req, res) {
    res.redirect('/login');
    //res.status(200).send("Hello World!");
});
/**
router.get('/event/:id', function (req, res) {
    models.events.findOne({ _id: req.params.id }, function (err, event) {
        if (err || !event) next(err);
        else {
            models.eventRecords.find({ event: event._id }).populate("user").exec(function (err, eventRecords) {
                res.render('event', { event: event, eventRecords: eventRecords, title: 'Showing Event ' + event.name });
            });
        }
    });
});

router.get('/user/:id', function (req, res) {
    models.users.findOne({ _id: req.params.id }, function (err, user) {
        if (err || !user) next(err);
        else {
            models.eventRecords.find({ user: user._id }).populate("event").exec(function (err, eventRecords) {
                res.render('user', { user: user, eventRecords: eventRecords, title: 'Showing User ' + user.engName });
            });
        }
    });
});
**/
router.get('/user/:id/delete', function (req, res) {
    models.users.remove({ _id: req.params.id }, function (err) {
        if (err) next(err);
        else {
            res.redirect('/');
        }
    });
});

router.get('/eventRecords/:id/delete', function (req, res) {
    models.eventRecords.remove({ _id: req.params.id }, function (err) {
        if (err) next(err);
        else {
            res.redirect('/');
        }
    });
});
/**
router.post('/addEvent', function (req, res) {
    var event = new models.events(req.body);
    event.token = models.events.token();
    event.save(function (err, event) {
        if (err) {
            next(err);
        } else {
            res.redirect('/event/' + event._id);
        }
    });
});
**/

router.get('/qr/:id', function (req, res, next) {
    models.eventSessions.findOne({ _id: req.params.id }).populate("event").exec(function(err, session) {
        if(err) next(err);
        else if(!session) next();
        else {
            res.render('qr', { session: session });
        }
    });
});

router.get('/qr/checkin/:id', function (req, res) {
    var qr_svg = qr.image('http://' + req.hostname + ':' + config.port + '/checkin/' + req.params.id, { type: 'png', margin: 0 });
    qr_svg.pipe(res);
});

router.get('/qr/checkout/:id', function (req, res) {
    var qr_svg = qr.image('http://' + req.hostname + ':' + config.port + '/checkout/' + req.params.id, { type: 'png', margin: 0 });
    qr_svg.pipe(res);
});

router.get('/checkin/:id', function (req, res) {
    res.send('Please download the app');
});

router.get('/checkout/:id', function (req, res) {
    res.send('Please download the app');
});

/**
router.post('/login', function (req, res) { 
    res.redirect('/console.html');
});

router.get('/logout', function (req, res) {
    res.redirect('/login.html');
});
**/

// Set login/ logout mechanism
router.get('/login', function (req, res) {
    res.render('login', { user: req.user });
});

router.post('/login',
  passport.authenticate('local', { failureRedirect: 'login' }),
  function (req, res) {
    res.render('console', { user: req.user });
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/login');
});

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { res.cookie('username', req.user.username); return next(); }
    else { res.redirect('/login'); }
}

module.exports = router;