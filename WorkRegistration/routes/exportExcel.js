﻿var express = require('express');
var router = express.Router();
var models = require('../models.js');
var moment = require('moment');
var async = require('async');
var xlsx = require('node-xlsx');

function prettyDate(date) {
    if(!date) return '';
    return new moment(date).format("DD-MMM-YYYY");
}

function prettyDate2(date) {
    if(!date) return '';
    return new moment(date).format("DD-MM-YYYY");
}

function prettyDate3(date) {
    if(!date) return '';
    return new moment(date).format("YYYY-MM-DD");
}


function prettyTime(date) {
    if(!date) return '';
    return new moment(date).format("HHmm");
}

function prettyTime2(date) {
    if(!date) return '';
    return new moment(date).format("HH:mm");
}

function nullp(item) {
    for(var i = 1; i < arguments.length; i++) {
        if(!item) return null;
        item = item[arguments[i]];
    }
    return item;
}

function userList(req, res, next) {

    var cond = {};
    if(req.params.school)
        cond["UST.school"] = req.params.school;

    models.users.find(cond).sort("UST.school gender name_eng").exec(function(err, users) {
        if(err) {
            next(err);
            return;
        }

        var rows = users.map(function(user) {
            return [
                user.name_eng,
                user.name_chi,
                user.UST.stid,
                user.phoneNo,
                user.UST.itsc,
                user.UST.yearOfStudy,
                user.email,
                user.UST.school,
                user.gender,
                user.UniYID,
                user.effectiveTo
            ];
        });

        rows.push([]);
        rows.push(['Total: ', rows.length - 1]);
        rows.push([]);
        rows.push(['End of listing']);

        rows.unshift([
                'Eng Name',
                'Chi Name',
                'student no',
                'Tel no',
                'ITSC', 
                'Year of study',
                'Preferred email',
                'School',
                'Gender',
                'Uni-Y membership no',
                'effective date to'
            ]);

        rows.unshift([]);
        rows.unshift(['School:', req.params.school || 'All']);
        rows.unshift(["Volunteer Listing"]);

        var excel = xlsx.build([{ name: "Volunteer List", data: rows }]);

        res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        var date = new moment().format('YYYYMMDD-hhmmss');
        res.setHeader('Content-disposition', 'attachment; filename=' + date + '-volunteer.xlsx');
        res.send(excel);
    });
}

router.get('/users/list', userList);
router.get('/users/list/:school', userList);




function eventList(req, res, next) {

    var cond = {};
    if(req.params.EventID)
        cond["EventID"] = req.params.EventID;

    models.events.find(cond).sort("EventID").exec(function(err, events) {
        if(err) {
            next(err);
            return;
        }

        var rows = [];

        async.eachSeries(events, function(event, cb) {
            models.eventSessions.find({event: event._id}).sort("SessionID").exec(function(err, eventSessions) {
                if(err) {
                    cb(err);
                    return;
                }

                for(var i = 0; i < eventSessions.length; i++) {
                    var eventSession = eventSessions[i];
                    rows.push([
                        event.EventID,
                        event.name_eng,
                        event.name_chi,
                        eventSession.SessionID,
                        eventSession.name_eng,
                        prettyDate(eventSession.startTime),
                        prettyDate(eventSession.endTime),
                        prettyTime(eventSession.startTime),
                        prettyTime(eventSession.endTime),
                        eventSession.maxHours,
                        eventSession.maxPoints
                    ]);
                }
                cb();
            });
        }, function(err) {
            if(err) {
                next(err);
                return;
            }

            rows.push([]);
            rows.push(['Total: ', rows.length - 1]);
            rows.push([]);
            rows.push(['End of listing']);

            rows.unshift([
                'Program code',
                'Program ENGLISH Name',
                'Program CHINESE Name',
                'Session_no',
                'Session name',
                'Session starting date',
                'Session ending date',
                'Session starting time',
                'Session ending time',
                'service hour entitled',
                'score entitled'
            ]);
            rows.unshift([]);
            rows.unshift(['Program code:', req.params.EventID || 'All']);
            rows.unshift(["Program Listing"]);

            var excel = xlsx.build([{ name: "Program List", data: rows }]);

            res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            var date = new moment().format('YYYYMMDD-hhmmss');
            res.setHeader('Content-disposition', 'attachment; filename=' + date + '-program.xlsx');
            res.send(excel);
        });
    });
}

router.get('/events/list', eventList);
router.get('/events/list/:EventID', eventList);






function eventRecordList(req, res, next) {

    var cond = {};
    if(req.params.stid)
        cond["UST.stid"] = req.params.stid;

    var startTime = req.params.startTime ? new moment(req.params.startTime).toDate() : null;
    var endTime = req.params.endTime ? new moment(req.params.endTime).toDate() : null;

    models.users.find(cond).sort("UST.stid").exec(function(err, users) {
        if(err) {
            next(err);
            return;
        }

        var rows = [];

        async.eachSeries(users, function(user, cb) {
            models.eventRecords.find({user: user._id}).populate({path: 'session', populate: { path:  'event', model: 'Event' }}).exec(function(err, eventRecords) {
                if(err) {
                    cb(err);
                    return;
                }

                async.eachSeries(eventRecords, function(eventRecord, cb) {
                        if((!startTime || eventRecord.startTime && eventRecord.startTime.getTime() >= startTime.getTime()) &&
                            (!endTime || eventRecord.endTime && eventRecord.endTime.getTime() <= endTime.getTime())) {
                            rows.push([
                                eventRecord.volunteer_no,
                                user.UST.stid,
                                user.name_eng,
                                user.name_chi,
                                nullp(eventRecord.session, 'event', 'EventID'),
                                nullp(eventRecord.session, 'event', 'name_eng'),
                                nullp(eventRecord.session, 'startTime'),
                                nullp(eventRecord.session, 'endTime'),
                                nullp(eventRecord.session, 'SessionID'),
                                prettyTime(nullp(eventRecord.records[0], 'joinTime')),
                                prettyTime(nullp(eventRecord.records[eventRecord.records.length - 1], 'leaveTime')),
                                eventRecord.restTime(),
                                eventRecord.hours,
                                eventRecord.points
                            ]);
                        }
                        cb();
                }, function(err) {
                    cb(err);
                });
            });
        }, function(err) {
            if(err) {
                next(err);
                return;
            }

            rows.push([]);
            rows.push(['Total: ', rows.length - 1]);
            rows.push([]);
            rows.push(['End of listing']);

            rows.unshift([
                'Volunteer No',
                'student no',
                'Student Eng Name',
                'Student Chi Name',
                'Program code',
                'Program ENG Name',
                'Session start date',
                'Session end datte',
                'Session No',
                'Sign in Time',
                'Sign out Time',
                'Rest Time',
                'Service hr. earned',
                'Score earned'
            ]);
            rows.unshift([]);
            if(endTime)
                rows.unshift(["TO: ", prettyDate2(endTime)]);
            if(startTime)
                rows.unshift(["FROM: ", prettyDate2(startTime)]);
            rows.unshift(['Student No:', req.params.stid || 'All']);
            rows.unshift(["Volunteer-Attendancy  Records"]);

            var excel = xlsx.build([{ name: "Volunteer-Attendancy", data: rows }]);

            res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            var date = new moment().format('YYYYMMDD-hhmmss');
            res.setHeader('Content-disposition', 'attachment; filename=' + date + '-attendance.xlsx');
            res.send(excel);
        });
    });
}

router.get('/eventRecords/list', eventRecordList);
router.get('/eventRecords/list/:startTime/:endTime/:stid', eventRecordList);






function eventRegList(req, res, next) {

    var cond = {};
    if(req.params.EventID)
        cond["EventID"] = req.params.EventID;

    models.events.find(cond).sort("EventID").exec(function(err, events) {
        if(err) {
            next(err);
            return;
        }

        var rows = [];

        async.eachSeries(events, function(event, cb) {
            models.eventSessions.find({event: event._id}).exec(function(err, eventSessions) {
                if(err) {
                    cb(err);
                    return;
                }

                    models.eventRecords.find({session: { $in: eventSessions.map(function(x) { return x._id }) } }, function(err, eventRecords) {

                        if(err) {
                            cb(err);
                            return;
                        }

                        models.users.find({_id: { $in: eventRecords.map(function(x) { return x.user; }) }}).sort('name_eng').exec(function(err, users) {
                            if(err) {
                                cb(err);
                                return;
                            }

                            for(var i = 0; i < users.length; i++) {
                                var user = users[i];
                                var eventRecord = eventRecords.filter(function(x) { return x.user.toString() == user._id.toString() })[0];
                                rows.push([
                                    eventRecord.volunteer_no,
                                    user.name_eng,
                                    user.name_chi,
                                    user.name_nick,
                                    user.UST.stid,
                                    user.phoneNo,
                                    user.UST.itsc,
                                    user.email,
                                    user.UST.school,
                                    user.gender,
                                    user.UniYID,
                                    prettyDate(user.effectiveTo),
                                    user.EmergencyContact.Name,
                                    user.EmergencyContact.Tel,
                                ]);
                            }
                            cb();

                        });
                    });

            });
        }, function(err) {
            if(err) {
                next(err);
                return;
            }

            rows.push([]);
            rows.push(['Total: ', rows.length - 1]);
            rows.push([]);
            rows.push(['End of listing']);

            rows.unshift([
                'Volunteer No',
                'Eng Name',
                'Chi Name',
                'Nick Name',
                'student no',
                'Tel no',
                'ITSC',
                'Preferred email',
                'School',
                'Gender',
                'Uni-Ymembership no',
                'effective date to',
                'Emergency Contact Name',
                'Emergency Contact No'
            ]);
            rows.unshift([]);
            rows.unshift(['Program code:', req.params.EventID || 'All']);
            rows.unshift(["Volunteer Checklist"]);

            var excel = xlsx.build([{ name: "Volunteer Checklist", data: rows }]);

            res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            var date = new moment().format('YYYYMMDD-hhmmss');
            res.setHeader('Content-disposition', 'attachment; filename=' + date + '-checklist.xlsx');
            res.send(excel);
        });
    });
}

router.get('/eventRegs/list', eventRegList);
router.get('/eventRegs/list/:EventID', eventRegList);







function fullList(req, res, next) {

    models.eventRecords.find({}).populate({path: 'session', populate: { path:  'event', model: 'Event' }}).populate("user").exec(function(err, eventRecords) {
        if(err) {
            next(err);
            return;
        }

        eventRecords = eventRecords.sort(function(a, b) {
            if((!a.session || !a.session.event) || (!b.session || !b.session.event)) {
                if((!a.session || !a.session.event) && (!b.session || !b.session.event))
                    return 0;
                else if((!a.session || !a.session.event)) return 1;
                else return -1;
            }
            if(a.session.event.EventID != b.session.event.EventID)
                return a.session.event.EventID > b.session.event.EventID ? 1 : -1;
            if(a.session.startTime && b.session.startTime && a.session.startTime.getTime() != b.session.startTime.getTime())
                return a.session.startTime.getTime() > b.session.startTime.getTime() ? 1 : -1;
            if(a.session.SessionID != b.session.SessionID)
                return a.session.SessionID > b.session.SessionID ? 1 : -1;
            if(a.user.name_eng != b.user.name_eng)
                return a.user.name_eng > b.user.name_eng ? 1 : -1;
            return 0;
        });

        var rows = eventRecords.map(function(eventRecord) {
            return [
                eventRecord.volunteer_no,

                nullp(eventRecord.user, 'name_eng'),
                nullp(eventRecord.user, 'name_chi'),
                nullp(eventRecord.user, 'UST', 'stid'),
                nullp(eventRecord.user, 'phoneNo'),
                nullp(eventRecord.user, 'UST', 'itsc'),
                nullp(eventRecord.user, 'email'),
                nullp(eventRecord.user, 'gender'),
                nullp(eventRecord.user, 'UST', 'school'),
                nullp(eventRecord.user, 'UniYID'),
                prettyDate3(nullp(eventRecord.user, 'effectiveTo')),
                nullp(eventRecord.session, 'event', 'EventID'),
                nullp(eventRecord.session, 'event', 'name_eng'),
                prettyDate(nullp(eventRecord.session, 'startTime')),
                prettyTime2(nullp(eventRecord.session, 'startTime')),
                prettyTime2(nullp(eventRecord.session, 'endTime')),
                prettyTime2(nullp(eventRecord.records[0], 'joinTime')),
                prettyTime2(nullp(eventRecord.records[eventRecord.records.length - 1], 'leaveTime')),

                nullp(eventRecord.session, 'maxHours'),
                nullp(eventRecord.session, 'maxPoints'),
                eventRecord.hours,
                eventRecord.points,

                nullp(eventRecord.user, 'EmergencyContact', 'Name'),
                nullp(eventRecord.user, 'EmergencyContact', 'Relationship'),
                nullp(eventRecord.user, 'EmergencyContact', 'Tel'),

            ];
        });

        rows.push([]);
        rows.push(['Total: ', rows.length - 1]);
        rows.push([]);
        rows.push(['End of listing']);

        rows.unshift([
                'Volunteer No','Eng Name','Chi Name','student no','Tel no','ITSC','Preferred email ','Gender','School','membership no','effective date to','Program code','Program Name','Proram Date','Program starting time','Program ending time','sign in time','sign out time','service hour entitled','score entitled','service hour earned','score earned','Emergency Contact Person','Relationship','Emergency Contact Number'
            ]);

        rows.unshift([]);
        rows.unshift(["Full Listing"]);

        var excel = xlsx.build([{ name: "Full List", data: rows }]);

        res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        var date = new moment().format('YYYYMMDD-hhmmss');
        res.setHeader('Content-disposition', 'attachment; filename=' + date + '-full.xlsx');
        res.send(excel);
    });
}

router.get('/full/list', fullList);



module.exports = router;
