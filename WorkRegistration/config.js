﻿

module.exports = {
    sessionSecret: "workregistration201603100203",
    hashSecret: "workregistration201603100203",
    passwordSecret: "workregistration201603100203",
    db: 'mongodb://localhost:27017/workregistration',
    appName: "Work Registration",
    port: 1337,
    mongo_express: {
        mongodb: {
            admin: true,
            server: 'localhost',
            port: 27017,
            adminUsername: '',
            adminPassword: '',
            whitelist: [],
            blacklist: [],
        },
        site: {
        },
        options: {
            documentsPerPage: 20,
        },
    }
};
