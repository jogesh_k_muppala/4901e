﻿var mongoose = require('mongoose');

module.exports = function (db) {

    var schema = mongoose.Schema({
        event: { type: mongoose.Schema.ObjectId, ref: 'Event' },
        user: { type: mongoose.Schema.ObjectId, ref: 'User' },
        no: String,
        amount: Number,
        date: Date,
        tee_size: String
    });

    var EventReg = mongoose.model('EventReg', schema);
    return EventReg;
};


