﻿
var mongoose = require('mongoose');
var uuid = require('node-uuid');
var moment = require('moment');

module.exports = function (db) {
    
    var schema = mongoose.Schema({
        EventID: { type: String, index: { unique: true }, sparse: true },
        name_eng: String,
        name_chi: String,
        disabled: { type: Boolean, default: false },
    });

    schema.methods.basic = function() {
        return {
            name_eng: this.name_eng,
            name_chi: this.name_chi
        };
    }
    
    var Event = mongoose.model('Event', schema);
    return Event;
};


