var mongoose = require('mongoose');
var uuid = require('node-uuid');

module.exports = function (db) {

    var schema = mongoose.Schema({
        name_eng: String,
        name_chi: String,
        years: { type: Number, default: 0 },
        disabled: { type: Boolean, default: false },
        points_total: { type: Number, default: 0 },
        points_peryr: { type: Number, default: 0 },
        //Has been assumed always level 3        
        points_subtotal: [
            {
                sub_level: { type: mongoose.Schema.ObjectId, ref: 'EventRank' },
                sub_total: { type: Number, default: 0 }
            }
        ],       
        prereq: { type: mongoose.Schema.ObjectId, ref: 'Rank' }
    });

    var Event = mongoose.model('Rank', schema);
    return Event;
};


