﻿
var mongoose = require('mongoose');
var uuid = require('node-uuid');

module.exports = function (db) {
    
    var schema = mongoose.Schema({
        SessionID: String,
        name_chi: String,
        name_eng: String,
        startTime: Date,
        endTime: Date,
        maxHours: Number,
        maxPoints: Number,
        location: String,
        lat: Number,
        lng: Number,
        checkInToken: { type: String, index: { unique: true }, default: uuid.v4 },
        checkOutToken: { type: String, index: { unique: true }, default: uuid.v4 },
        event: { type: mongoose.Schema.ObjectId, ref: 'Event' },
    });

    schema.statics.token = function () {
        return uuid.v4();
    }


    schema.methods.basic = function() {
        return {
            name_eng: this.name_eng,
            name_chi: this.name_chi,
            startTime: this.startTime,
            endTime: this.endTime,
            maxHours: this.maxHours,
            maxPoints: this.maxPoints,
            location: this.location,
            lat: this.lat,
            lng: this.lng,
            event: this.event ? this.event.basic() : null
        };
    }

    var EventSession = mongoose.model('EventSession', schema);
    return EventSession;
};


