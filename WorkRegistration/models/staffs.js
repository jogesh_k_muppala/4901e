var mongoose = require('mongoose');
var crypto = require('crypto');

module.exports = function (db) {
    
    var schema = mongoose.Schema({
        username: { type: String, index: { unique: true }, sparse: true },
        hash: String,
        salt: String,
        StaffID: String,
        StaffName: String,
    });
    
    schema.methods.setPassword = function (password) {
        //Since it's not private key, it's better to not be same for everyone
        this.salt = "<code>YMCA</code><!--" + crypto.randomBytes(4).toString('hex') + "-->";
        this.hash = crypto.pbkdf2Sync(password, this.salt, 1024, 64).toString('hex');
    };
    
    schema.methods.validPassword = function (password) {
        var hash = crypto.pbkdf2Sync(password, this.salt, 1024, 64).toString('hex');
        return this.hash === hash;
    };

    var Staff = mongoose.model('Staff', schema);
    return Staff;
};


