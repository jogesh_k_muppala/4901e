﻿/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
    BackAndroid,
    DrawerLayoutAndroid,
    ToolbarAndroid,
} from 'react-native';

import loading from './app/loading'
import qrcode from './app/qrcode';
import signin from './app/signin';
import events from './app/events'
import profile from './app/profile'
import about from './app/about'
import home from './app/home'

import navigationView from './app/navigationView';

class WorkRegistrationApp extends Component {
	
	constructor(props) {
		super(props);
		BackAndroid.addEventListener('hardwareBackPress', () => {
			if (this.nav && !this.route.top) {
				this.nav.pop();
				return true;
			}
			return false;
		});
	}
	
	render() {
		return (
		  <Navigator
			style={styles.container}
			initialRoute={routes.loading}
            routes = {routes}
            renderScene={(route, navigator) => {
                this.nav = navigator;
                navigator.routes = routes;
				this.route = route;
				if (route.component) {
				    if(route.noAction) {
				        return (
                            <route.component navigator={navigator} route={route}/>
                        );
				    }
                    var mdrawer;
				    return (<DrawerLayoutAndroid
				        drawerWidth={300}
				        ref={(drawer) => mdrawer = drawer}
				        drawerPosition={DrawerLayoutAndroid.positions.Left}
				        renderNavigationView={() => navigationView(navigator)}>
                            <ToolbarAndroid
                                style={styles.toolbar}
                                navIcon={require('./app/ic_menu.png')}
                                title={route.title}
                                titleColor="#fff"
                                onIconClicked={() => mdrawer.openDrawer()}
                            />
                            <route.component navigator={navigator} route={route}/>
                        </DrawerLayoutAndroid>
                    );
				}
            }}
			/>
		);
	}
}

const styles = StyleSheet.create({
    title: { fontWeight: '500', },
    toolbar: { height: 56, backgroundColor: '#1D92F2', },
}); 

const routes = {
    loading: {
        title: 'Loading',
        component: loading,
        noAction: true,
        top: true,
    },
    signin: {
        title: 'Sign In',
        component: signin,
        noAction: true,
        top: true
    },
    home: {
        title: 'YMCA',
        component: home,
        top: true
    },
    qrcode: {
        title: 'Scan QR Code',
        component: qrcode,
        noAction: true,
    },
    events: {
        title: 'History',
        component: events,
    },
    profile: {
        title: 'Profile',
        component: profile,
    },
    about: {
        title: 'About',
        component: about,
    },
};

AppRegistry.registerComponent('WorkRegistrationApp', () => WorkRegistrationApp);
