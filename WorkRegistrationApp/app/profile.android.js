



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  DrawerLayoutAndroid
} from 'react-native';

var AwesomeButton = require('react-native-awesome-button')

import api from './api'
import navigationView from './navigationView';

class Profile extends Component {
	
	constructor(props) {
		super(props);
		this.state = { 
			pass: '',
			new: '',
			new2: ''
		};
	}
	
	async _change() {
		if(this.state.new == this.state.new2) {
			var result = await api.changePassword(this.state.pass, this.state.new);
			if(result.status) {
				this.props.navigator.pop();
			} else {
				alert('cannot change password');
			}
		} else {
			alert('password mismatch');
		}
	}

	render() {
		return (
            <DrawerLayoutAndroid
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView()}
            >
			    <View>
				    <Text style={styles.title}>Old Password: </Text>
				    <TextInput
                      style = {styles.textinput }
                      ref= "pass"
				      secureTextEntry={true}
                      onChangeText={(text) => this.setState({pass: text})}
                      value={this.state.pass}
                    />
				    <Text style={styles.title}>New Password: </Text>
				    <TextInput
                      style = { styles.textinput }
                      ref= "new"
				      secureTextEntry={true}
                      onChangeText={(text) => this.setState({new: text})}
                      value={this.state.new}
                    />
				    <Text style={styles.title}>New Password (Retype): </Text>
				    <TextInput
                      style = { styles.textinput }
                      ref= "new2"
				      secureTextEntry={true}
                      onChangeText={(text) => this.setState({new2: text})}
                      value={this.state.new2}
                    />
					
					<View style={styles.container}>
						<AwesomeButton  backgroundStyle={styles.registerButtonBackground}
						abelStyle={styles.registerButtonLabel}
						transitionDuration={200}
						states={{
							default: {
							text: 'Change',
							onPress: this._change.bind(this),
							backgroundColor: '#2196F3',
							}
						}}
						/>
					</View>	

			    </View>
            </DrawerLayoutAndroid>
		);
	}
}

const styles = StyleSheet.create({
    title: { fontWeight: '500', marginLeft: 10, fontSize:15},
    textinput: { marginLeft: 10, marginRight: 10,paddingBottom: 10 },
	container: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		margin: 20
	},
	changeButtonBackground: {
		flex: 1,
		height: 40,
		width: 250, 
		borderRadius: 10
	},
	changeButtonLabel: {
		color: 'white'
	},
});


export default Profile

