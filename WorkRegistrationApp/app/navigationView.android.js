﻿'use strict';
import React, {
  Component,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  DrawerLayoutAndroid,
  TouchableNativeFeedback
} from 'react-native';

import api from './api'

function navigationView(nav) {
    return (
        <View style={{flex: 1, backgroundColor: '#F2F2F2'}}>
            <View style={{backgroundColor: '#F55344', paddingLeft: 10, paddingRight: 10, paddingTop: 20, paddingBottom: 20}}>
				<Text style={styles.profile}>
					<Text style={styles.title}>Name: </Text>
					{api.user().name_eng}
				</Text>
				<Text style={styles.profile}>
					<Text style={styles.title}>ITSC: </Text>
					{api.user().itsc}
				</Text>
            </View>
            <View style={{}}>
                <View style={styles.item}>
                    <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={() => nav.replace(nav.routes.home)}>
                        <View style={styles.buttonContainer}>
                            <Image style={styles.button} source={require('./home.png')} />
                            <Text style={styles.buttonText}>Home</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={() => nav.push(nav.routes.qrcode)}>
                        <View style={styles.buttonContainer}>
                            <Image style={styles.button} source={require('./qr.png')} />
                            <Text style={styles.buttonText}>QR Code Scanner</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={() => nav.push(nav.routes.history)}>
                        <View style={styles.buttonContainer}>
                            <Image style={styles.button} source={require('./history.png')} />
                            <Text style={styles.buttonText}>History</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={() => nav.push(nav.routes.profile)}>
                        <View style={styles.buttonContainer}>
                            <Image style={styles.button} source={require('./profile.png')} />
                            <Text style={styles.buttonText}>Profile</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={() => nav.push(nav.routes.about)}>
                        <View style={styles.buttonContainer}>
                            <Image style={styles.button} source={require('./about.png')} />
                            <Text style={styles.buttonText}>About</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={async () => { await api.logout(); nav.replace(nav.routes.signin); }}>
                        <View style={styles.buttonContainer}>
                            <Image style={styles.button} source={require('./logout.png')} />
                            <Text style={styles.buttonText}>Logout</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
	title: { fontWeight: '500', },
    view: {alignItems: 'center', paddingTop: 20, flex: 1},
    icon: { width: 300, height: 250, alignItems: 'center', marginBottom: 20 },
    button: { width: 32, height: 32, marginRight: 20 },
    row: { flexDirection:'row', },
    profile: { fontSize: 16, marginTop: 10, marginBottom: 10 },
    buttonText: { fontSize: 16, marginTop: 4 },
    buttonContainer: {flex: 1, flexDirection: 'row', paddingTop: 10, paddingBottom: 10, paddingLeft:20, paddingRight: 20},
});


export default navigationView;

