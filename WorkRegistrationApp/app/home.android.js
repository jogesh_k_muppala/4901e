
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TouchableNativeFeedback,
  ToolbarAndroid,
  DrawerLayoutAndroid,
  ScrollView 
} from 'react-native';

import api from './api'

import navigationView from './navigationView';

var Button = require('react-native-button');

class Home extends Component {
	
	constructor(props) {
		super(props);
		this.state = { 
			initialPosition: 'unknown',
			lastPosition: 'unknown',
		};
		this.watchID = null;
	}
	
	componentDidMount() {
		this.setState({initialPosition: 'loading', lastPosition:'loading'});
		navigator.geolocation.getCurrentPosition(
			(position) => { 
				if(!this.state.initialPosition) {
					var initialPosition = JSON.stringify(position);
					this.setState({initialPosition});
				}
			},(error) => alert(error.message), 
			{enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
		);
		this.watchID = navigator.geolocation.watchPosition(
			(position) => {
				var lastPosition = JSON.stringify(position);
				this.setState({lastPosition});
				if(!this.state.initialPosition) {
					this.setState({initialPosition: lastPosition});
				}
			}); 
	}
		
	componentWillUnmount() {
		navigator.geolocation.clearWatch(this.watchID);
	}
	
	_scanQR() {
        this.props.navigator.push(this.props.navigator.routes.qrcode);
	}

	
	_showEvents() {
        this.props.navigator.push(this.props.navigator.routes.events);
	}
	
	_profile() {
        this.props.navigator.push(this.props.navigator.routes.profile);
	}
	
	_about() {
        this.props.navigator.push(this.props.navigator.routes.about);
	}
	
	render() {
		return (
            <ScrollView>
			    <View style={styles.view}>
                    <Image style={styles.icon} source={require('./logo.png')} />

                    <View style={styles.row}>
                        <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={this._scanQR.bind(this)}>
                            <View style={styles.buttonContainer}>
                                <Image style={styles.button} source={require('./qr.png')} />
                                <Text style={styles.buttonText}>QR Code Scanner</Text>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={this._showEvents.bind(this)}>
                            <View style={styles.buttonContainer}>
                                <Image style={styles.button} source={require('./history.png')} />
                                <Text style={styles.buttonText}>History</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>

                    <View style={styles.row}>
                        <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={this._profile.bind(this)}>
                            <View style={styles.buttonContainer}>
                                <Image style={styles.button} source={require('./profile.png')} />
                                <Text style={styles.buttonText}>Profile</Text>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback style={styles.tbutton} background={TouchableNativeFeedback.SelectableBackground()} onPress={this._about.bind(this)}>
                            <View style={styles.buttonContainer}>
                                <Image style={styles.button} source={require('./about.png')} />
                                <Text style={styles.buttonText}>About</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
			    </View>
            </ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	title: { fontWeight: '500', },
    view: {alignItems: 'center', paddingTop: 20, flex: 1},
    icon: { width: 232, height: 200, alignItems: 'center', marginBottom: 20 },
    button: { width: 100, height: 100, marginBottom: 10 },
    buttonContainer: { paddingLeft: 30, paddingRight: 30, paddingTop: 20, paddingBottom: 20, alignItems: 'center', flex: 1 },
    buttonText: { fontSize: 16 },
    tbutton: { borderRadius:10 },
    row: { flexDirection:'row', },
    toolbar: { height: 56, backgroundColor: '#1D92F2', },
});


export default Home

