
import {
	AsyncStorage
} from 'react-native';

class api {
	constructor() {
		this._loadInitialState().done();
	}
	
	async call(cmd, body) {
		await this._loadInitialState();
		body = body || {};
		body.token = this.userInfo.token;
		var response = await fetch('http://hyk2d.hopto.org:3010/api' + cmd, {
			method: 'post',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(body)
		});
		//console.warn(await response.text());
		return await response.json();
	}
	
	async _loadInitialState() {
		if(this._inited) return;
		try {
			var json = await AsyncStorage.getItem("userInfo");
			if(json)
				this.userInfo = JSON.parse(json);
			console.log(this.userInfo);
		}
		catch (error) { 
			console.warn('AsyncStorage error: ' + error.message); 
		}
		this.userInfo =  this.userInfo || {};
		this._inited = true;
	}
	
	async saveState() {
		try {
			await AsyncStorage.setItem("userInfo", JSON.stringify(this.userInfo));
		}
		catch (error) { 
			console.warn('AsyncStorage error: ' + error.message); 
		}
	}
	
	async register(form) {
		var result = await this.call('/register', form);
		if(result.status && result.token) {
			this.userInfo.token = result.token;
            this.userInfo.user = result.user;
			await this.saveState();
		}
		return result;
	}
	
	async signin(email, pass) {
		var result = await this.call('/signin', { email: email, pass: pass });
		if(result.status && result.token) {
			this.userInfo.token = result.token;
            this.userInfo.user = result.user;
			await this.saveState();
		}
		return result;
	}
	
	async checkToken() {
		var result = await this.call('/checkToken');
        if(result.status) {
            this.userInfo.user = result.user;
        }
		return result;
	}
	
	async check(email) {
		var result = await this.call('/check', { email: email });
        if(result.status) {
            this.userInfo.user = result.user;
        }
        return result;
	}
	
	async checkin(event) {
		return await this.call('/checkin', { event: event });
	}
	
	async checkout(event) {
		return await this.call('/checkout', { event: event });
	}

	async events() {
		return await this.call('/events');
	}
	
	async changePassword(pass, $new) {
		return await this.call('/changePassword', { pass: pass, new: $new });
	}
	
	async logout() {
		delete this.userInfo.token;
		await this.saveState();
	}

    user() {
        if(this.userInfo && this.userInfo.user) return this.userInfo.user;
        return {};
    }
}


export default new api()