﻿

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableHighlight,
  TouchableNativeFeedback
} from 'react-native';

var Button = require('react-native-button');
import api from './api'

class Loading extends Component {
	
	constructor(props) {
		super(props);
        this._checkUser().done();
	}

	async _checkUser() {
		if(!(await api.checkToken()).status) {
			this.props.navigator.replace(this.props.navigator.routes.signin);
		} else {
			this.props.navigator.replace(this.props.navigator.routes.home);
        }
	}
	
	render() {
		return (
			<View style={styles.view}>
                    <Image style={styles.icon} source={require('./logo.png')} />
				    <Text style={styles.loading}>Loading...</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	title: { fontWeight: '500', },
    view: {alignItems: 'center', flex: 1, justifyContent: 'center',},
    icon: { width: 290, height: 250 },
    loading: { fontSize: 20, marginTop: 10 },
});


export default Loading

